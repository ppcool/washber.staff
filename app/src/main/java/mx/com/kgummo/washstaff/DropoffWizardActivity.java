package mx.com.kgummo.washstaff;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.desai.vatsal.mydynamictoast.MyDynamicToast;
import com.google.gson.Gson;
import com.rilixtech.materialfancybutton.MaterialFancyButton;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import me.panavtec.wizard.Wizard;
import me.panavtec.wizard.WizardListener;
import me.panavtec.wizard.WizardPage;
import me.panavtec.wizard.WizardPageListener;
import mx.com.kgummo.washstaff.Adaptadores.adaTelefonos;
import mx.com.kgummo.washstaff.Adaptadores.adaTiposIncidencia;
import mx.com.kgummo.washstaff.Clases.clsDwConfirmacion;
import mx.com.kgummo.washstaff.Clases.clsDwDetalle;
import mx.com.kgummo.washstaff.Clases.clsDwPrendas;
import mx.com.kgummo.washstaff.Clases.clsGeneralCliente;
import mx.com.kgummo.washstaff.Clases.clsGlobales;
import mx.com.kgummo.washstaff.Clases.clsTelefono;
import mx.com.kgummo.washstaff.Clases.clsTipoIncidencia;
import mx.com.kgummo.washstaff.WebService.ws_GeneralClienteResult;
import mx.com.kgummo.washstaff.WebService.ws_GenericIntRequest;
import mx.com.kgummo.washstaff.WebService.ws_GenericResult;
import mx.com.kgummo.washstaff.WebService.ws_IncidenciaPickRequest;
import mx.com.kgummo.washstaff.WebService.ws_TelefonoResult;
import mx.com.kgummo.washstaff.WebService.ws_TipoIncidenciaResult;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class DropoffWizardActivity extends AppCompatActivity implements WizardPageListener, WizardListener {

    private Wizard wPickUp;
    clsTipoIncidencia oTipoSeleccion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dropoff_wizard);

        wPickUp = new Wizard.Builder(this,new clsDwDetalle(), new clsDwPrendas()
                ,new clsDwConfirmacion())
                .pageListener(this)
                .wizardListener(this)
                .build();

        wPickUp.init();
    }

    public Wizard getWizard(){return wPickUp;}

    @Override
    public void onPageChanged(int currentPageIndex, WizardPage page){}

    @Override
    public void onWizardFinished(){}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.men_dropoff,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if(id == R.id.optDatosCliente)
        {
            DatosCliente();
        }
        else if (id == R.id.optReportarIncidencia)
        {
            RegistroIncidencia();

        }
        return super.onOptionsItemSelected(item);
    }

    private void DatosCliente()
    {
        GeneralClientData oGeneral = new GeneralClientData();
        oGeneral.execute();
    }

    public class GeneralClientData extends AsyncTask<Void,Void,Boolean>
    {
        private ProgressDialog pDialog;
        private String result;

        GeneralClientData(){};

        @Override
        protected void onPreExecute()
        {
            pDialog = new ProgressDialog(DropoffWizardActivity.this);
            pDialog.setTitle("WashBer");
            pDialog.setMessage("Procesando...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... params)
        {
            try
            {
                String url = clsGlobales.API_URL + clsGlobales.API_CGENERALSERVICIO;
                OkHttpClient client = new OkHttpClient();
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                Gson h = new Gson();
                ws_GenericIntRequest oRequest = new ws_GenericIntRequest();

                oRequest.Valor = clsGlobales.gblServicioHeader.IdOrden;

                String jsRequest = h.toJson(oRequest);
                RequestBody body = RequestBody.create(JSON, jsRequest);
                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                return true;
            }
            catch (Exception e) {
                pDialog.dismiss();
                MyDynamicToast.errorMessage(getApplication(), "Tuvimos un problema en procesar la información, intenta nuevamente.");
                return false;
            }
        }

        protected void onPostExecute(final Boolean success)
        {
            ws_GeneralClienteResult oResult;
            clsGeneralCliente oGeneral;
            clsTelefono oTelefono;
            ws_TelefonoResult oTelefonoResult;
            try
            {
                oResult =new Gson().fromJson(String.valueOf(result),ws_GeneralClienteResult.class);
                if(success)
                {
                    if(oResult.Codigo == 100)
                    {
                        oGeneral = new clsGeneralCliente();
                        oGeneral.Correo = oResult.Correo;
                        oGeneral.FActivacion = oResult.FActivacion;
                        oGeneral.FNacimiento = oResult.FNacimiento;
                        oGeneral.FRegistro = oResult.FRegistro;
                        oGeneral.Fotografia = oResult.Fotografia;
                        oGeneral.Nombre = oResult.Nombre;
                        oGeneral.Telefonos = new ArrayList<clsTelefono>();
                        for(Integer i = 0; i < oResult.Telefonos.size(); i++)
                        {
                            oTelefonoResult = oResult.Telefonos.get(i);
                            oTelefono = new clsTelefono();
                            oTelefono.IdTelefonoCliente = oTelefonoResult.IdTelefonoCliente;
                            oTelefono.Telefono = oTelefonoResult.Telefono;
                            oTelefono.Titulo = oTelefonoResult.Titulo;

                            oGeneral.Telefonos.add(oTelefono);
                        }

                        clsGlobales.gblGeneralCliente =oGeneral;
                        pDialog.dismiss();
                        DisplayGeneralData();
                    }
                    else if(oResult.Codigo == 102)
                    {
                        pDialog.dismiss();
                        MyDynamicToast.informationMessage(getApplication(), oResult.Mensaje);
                    }
                    else
                    {
                        pDialog.dismiss();
                        MyDynamicToast.errorMessage(getApplication(), oResult.Mensaje);
                    }
                }
                else
                {
                    pDialog.dismiss();
                    MyDynamicToast.errorMessage(getApplication(), "Tuvimos un problema en procesar la información, intenta nuevamente.");
                }
            }
            catch (Exception ex)
            {
                pDialog.dismiss();
                MyDynamicToast.errorMessage(getApplication(), "Tuvimos un problema en procesar la información, intenta nuevamente.");
            }
        }

    }

    private void DisplayGeneralData()
    {
        TextView texto;
        ImageView imgCliente;
        final Dialog dialog  = new Dialog(this);
        clsGeneralCliente oGeneral = clsGlobales.gblGeneralCliente;
        adaTelefonos adapter;
        ListView listView;

        dialog.setContentView(R.layout.layout_modalgeneralcliente);

        MaterialFancyButton dismissButton = (MaterialFancyButton) dialog.findViewById(R.id.cmdCerrar);
        dismissButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        texto = (TextView) dialog.findViewById(R.id.txtNombre);
        texto.setText(oGeneral.Nombre);

        imgCliente = (ImageView) dialog.findViewById(R.id.imgFoto);
        if(oGeneral.Fotografia.equals(""))
        {
            imgCliente.setVisibility(View.GONE);
        }
        else
        {
            Picasso.with(this).load(oGeneral.Fotografia).into(imgCliente);
            imgCliente.setVisibility(View.VISIBLE);
        }

        texto = (TextView) dialog.findViewById(R.id.lblCorreo);
        if(oGeneral.Correo.equals(""))
        {
            texto.setVisibility(View.GONE);
        }
        else {
            texto.setText(oGeneral.Correo);
            texto.setVisibility(View.VISIBLE);
        }

        listView = (ListView) dialog.findViewById(R.id.lstTelefonos);
        adapter = new adaTelefonos(DropoffWizardActivity.this,oGeneral.Telefonos);

        listView.setAdapter(adapter);

        dialog.setTitle("Cliente");
        dialog.setCancelable(false);
        dialog.show();

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        //lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private void RegistroIncidencia()
    {
        GetTiposIncidencia oGet = new GetTiposIncidencia();
        oGet.execute();
    }

    public class GetTiposIncidencia extends AsyncTask<Void,Void,Boolean>
    {
        private ProgressDialog pDialog;
        private String result;

        GetTiposIncidencia(){};

        @Override
        protected void onPreExecute()
        {
            pDialog = new ProgressDialog(DropoffWizardActivity.this);
            pDialog.setTitle("WashBer");
            pDialog.setMessage("Procesando...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... params)
        {
            try
            {
                String url = clsGlobales.API_URL + clsGlobales.API_TIPOSINCIDENCIA;
                OkHttpClient client = new OkHttpClient();
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                Gson h = new Gson();
                ws_GenericIntRequest oRequest = new ws_GenericIntRequest();

                oRequest.Valor = clsGlobales.gblServicioHeader.IdOrden;

                String jsRequest = h.toJson(oRequest);
                RequestBody body = RequestBody.create(JSON, jsRequest);
                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                return true;
            }
            catch (Exception e) {
                pDialog.dismiss();
                MyDynamicToast.errorMessage(getApplication(), "Tuvimos un problema en procesar la información, intenta nuevamente.");
                return false;
            }
        }

        protected void onPostExecute(final Boolean success)
        {
            clsTipoIncidencia oTipo;
            ws_TipoIncidenciaResult oTipos;
            try
            {
                ws_TipoIncidenciaResult oResult = new Gson().fromJson(String.valueOf(result),ws_TipoIncidenciaResult.class);
                if(success)
                {
                    if(oResult.Codigo == 100)
                    {
                        oTipos = new ws_TipoIncidenciaResult();
                        oTipos.Incidencias = new ArrayList<clsTipoIncidencia>();
                        for(Integer i = 0; i < oResult.Incidencias.size(); i++)
                        {
                            oTipo = new clsTipoIncidencia();
                            oTipo = oResult.Incidencias.get(i);

                            oTipos.Incidencias.add(oTipo);
                        }

                        clsGlobales.gblTiposIncidencia = oTipos;
                        pDialog.dismiss();
                        DiaplayIncidenciaModal();
                    }
                    else if(oResult.Codigo == 101)
                    {
                        MyDynamicToast.errorMessage(DropoffWizardActivity.this, oResult.Mensaje);
                        pDialog.dismiss();
                    }
                    else
                    {
                        MyDynamicToast.warningMessage(DropoffWizardActivity.this, oResult.Mensaje);
                        pDialog.dismiss();
                    }
                }
            }
            catch (Exception ex)
            {
                pDialog.dismiss();
                MyDynamicToast.errorMessage(getApplication(), "Tuvimos un problema en procesar la información, intenta nuevamente.");
            }
        }
    }

    private void DiaplayIncidenciaModal()
    {
        Spinner cmbIncidencias;
        adaTiposIncidencia adapter;
        final Dialog dialog  = new Dialog(this);
        TextView texto;
        MaterialFancyButton boton;
        dialog.setContentView(R.layout.layout_modalincidencia);
        final EditText inputText;

        texto = (TextView) dialog.findViewById(R.id.lblFolio);
        texto.setText("Folio: " + clsGlobales.gblServicioHeader.Folio);

        adapter = new adaTiposIncidencia(DropoffWizardActivity.this,clsGlobales.gblTiposIncidencia.Incidencias);
        cmbIncidencias = (Spinner) dialog.findViewById(R.id.cmbMotivos);
        cmbIncidencias.setAdapter(adapter);
        cmbIncidencias.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id)
            {
                oTipoSeleccion = clsGlobales.gblTiposIncidencia.Incidencias.get(pos);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent){    }
        });

        inputText = (EditText)  dialog.findViewById(R.id.txtObservaciones);

        boton = (MaterialFancyButton) dialog.findViewById(R.id.cmdCancelar);
        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        boton = (MaterialFancyButton) dialog.findViewById(R.id.cmdAceptar);
        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //IncidenciaValidation(inputText.getText().toString());
                SaveIncidencia oSaveI = new SaveIncidencia(inputText.getText().toString());
                oSaveI.execute();
            }
        });

        dialog.setTitle("Incidencia");
        dialog.setCancelable(false);
        dialog.show();

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        //lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    public class SaveIncidencia extends AsyncTask<Void,Void,Boolean>
    {
        private ProgressDialog pDialog;
        private String result;
        private String observaciones;

        SaveIncidencia(String Observaciones)
        {
            this.observaciones = Observaciones;
        }

        @Override
        protected void onPreExecute()
        {
            pDialog = new ProgressDialog(DropoffWizardActivity.this);
            pDialog.setTitle("WashBer");
            pDialog.setMessage("Procesando...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected Boolean doInBackground(Void... params)
        {
            String url;
            url = clsGlobales.API_URL + clsGlobales.API_SAVEINCIDENCIADROP;

            OkHttpClient client = new OkHttpClient();
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            Gson h = new Gson();
            ws_IncidenciaPickRequest oRequest = new ws_IncidenciaPickRequest();

            try
            {
                oRequest.IdTipoIncidenciaPD = oTipoSeleccion.IdTipoIncidencia;
                oRequest.IdOrden = clsGlobales.gblServicioHeader.IdOrden;
                oRequest.IdRepartidor = clsGlobales.gblRepartidor.IdRepartidor;
                oRequest.Observaciones = this.observaciones;

                String jsRequest = h.toJson(oRequest);

                RequestBody body = RequestBody.create(JSON, jsRequest);
                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                return true;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                pDialog.dismiss();
                return false;
            }
        }

        protected void onPostExecute(final Boolean success)
        {
            try
            {
                ws_GenericResult oResult = new Gson().fromJson(String.valueOf(result),ws_GenericResult.class);
                if(success)
                {
                    if(oResult.Codigo == 100)
                    {
                        pDialog.dismiss();
                        MyDynamicToast.successMessage(DropoffWizardActivity.this, "Incidencia registrada correctamente");
                        Intent i = new Intent(DropoffWizardActivity.this,DropActivity.class);
                        startActivity(i);
                        finish();
                    }
                    else if(oResult.Codigo == 101)
                    {
                        pDialog.dismiss();
                        MyDynamicToast.errorMessage(DropoffWizardActivity.this, oResult.Mensaje);

                    }
                    else
                    {
                        pDialog.dismiss();
                        MyDynamicToast.warningMessage(DropoffWizardActivity.this, oResult.Mensaje);
                    }
                }
            }
            catch (Exception ex)
            {
                pDialog.dismiss();
                MyDynamicToast.errorMessage(getApplication(), "Tuvimos un problema en procesar la información, intenta nuevamente.");
            }
        }

    }
}
