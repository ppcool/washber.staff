package mx.com.kgummo.washstaff;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.desai.vatsal.mydynamictoast.MyDynamicToast;
import com.google.gson.Gson;
import com.rilixtech.materialfancybutton.MaterialFancyButton;

import mx.com.kgummo.washstaff.Clases.clsGlobales;
import mx.com.kgummo.washstaff.Clases.clsRepartidor;
import mx.com.kgummo.washstaff.WebService.ws_LoginRequest;
import mx.com.kgummo.washstaff.WebService.ws_RepartidorResult;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        MaterialFancyButton boton;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        boton = (MaterialFancyButton) findViewById(R.id.cmdLogin);
        boton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                ValidateData();
            }
        });
    }

    private void ValidateData()
    {
        EditText texto;
        String usuario;
        String password;

        texto = (EditText) findViewById(R.id.txtLogin);
        if(texto.getText().toString().trim().equals(""))
        {
            MyDynamicToast.warningMessage(getApplication(), "Especifica tu usuario");
            return;
        }
        usuario = texto.getText().toString().trim();

        texto = (EditText) findViewById(R.id.txtPassword);
        if(texto.getText().toString().trim().equals(""))
        {
            MyDynamicToast.warningMessage(getApplication(), "Especifica tu password");
            return;
        }

        password = texto.getText().toString().trim();
        ValidateUser validateUser = new ValidateUser(usuario,password);
        validateUser.execute();
    }

    public class ValidateUser extends AsyncTask<Void,Void,Boolean>
    {
        private ProgressDialog pDialog;
        private final String User;
        private final String Password;
        private String result;

        ValidateUser(String user,String psw)
        {
            this.User = user.trim();
            this.Password = psw.trim();
        }

        @Override
        protected void onPreExecute()
        {
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setTitle("WashBer");
            pDialog.setMessage("Procesando...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... params)
        {


            try
            {
                String url = clsGlobales.API_URL + clsGlobales.API_LOGINR;
                OkHttpClient client = new OkHttpClient();
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                Gson h = new Gson();
                ws_LoginRequest oReques = new ws_LoginRequest();

                oReques.Login = User;
                oReques.Password = Password;
                clsGlobales.gblLogin = User;
                clsGlobales.gblPassword = Password;

                String jsRequest = h.toJson(oReques);
                RequestBody body = RequestBody.create(JSON, jsRequest);

                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                return true;
            }
            catch (Exception e) {
                pDialog.dismiss();
                MyDynamicToast.errorMessage(getApplication(), "Tuvimos un problema en procesar la información, intenta nuevamente.");
                return false;
            }

        }

        protected void onPostExecute(final Boolean success)
        {
            ws_RepartidorResult oResult;
            try
            {
                oResult =new Gson().fromJson(String.valueOf(result),ws_RepartidorResult.class);
                if(success)
                {
                    if(oResult.Codigo == 100)
                    {
                        clsGlobales.gblRepartidor = new clsRepartidor();
                        clsGlobales.gblRepartidor.IdRepartidor = oResult.IdRepartidor;
                        clsGlobales.gblRepartidor.Nombre = oResult.Nombre;
                        clsGlobales.gblRepartidor.CorreoElectronico = oResult.CorreoElectronico;
                        clsGlobales.gblRepartidor.Fotografia = oResult.Fotografia;
                        clsGlobales.gblRepartidor.Proveedor = oResult.Proveedor;
                        clsGlobales.gblRepartidor.LogoProveedor = oResult.LogoProveedor;
                        clsGlobales.gblRepartidor.PendienteDrop = oResult.PendienteDrop;
                        clsGlobales.gblRepartidor.PendientePick = oResult.PendientePick;
                        clsGlobales.gblRepartidor.DropOff = oResult.DropOff;
                        clsGlobales.gblRepartidor.PickUp = oResult.PickUp;
                        clsGlobales.gblRepartidor.MontoMinimoServicio = oResult.MontoMinimoServicio;

                        pDialog.dismiss();
                        LogIn();
                    }
                    else if(oResult.Codigo == 102)
                    {
                        pDialog.dismiss();
                        MyDynamicToast.informationMessage(getApplication(), oResult.Mensaje);
                    }
                    else
                    {
                        pDialog.dismiss();
                        MyDynamicToast.errorMessage(getApplication(), oResult.Mensaje);
                    }
                }
                else
                {
                    pDialog.dismiss();
                    MyDynamicToast.errorMessage(getApplication(), "Tuvimos un problema en procesar la información, intenta nuevamente.");
                }
            }
            catch (Exception ex)
            {
                pDialog.dismiss();
                MyDynamicToast.errorMessage(getApplication(), "Tuvimos un problema en procesar la información, intenta nuevamente.");
            }
        }
    }

    public void LogIn()
    {
        Intent intent = new Intent(this,PrincipalActivity.class);
        startActivity(intent);
        finish();
    }
}
