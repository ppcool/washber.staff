package mx.com.kgummo.washstaff;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.desai.vatsal.mydynamictoast.MyDynamicToast;
import com.google.gson.Gson;
import com.rilixtech.materialfancybutton.MaterialFancyButton;
import com.squareup.picasso.Picasso;

import mx.com.kgummo.washstaff.Adaptadores.adaServicioheader;
import mx.com.kgummo.washstaff.Clases.clsDetalleServicio;
import mx.com.kgummo.washstaff.Clases.clsGlobales;
import mx.com.kgummo.washstaff.Clases.clsServicioH;
import mx.com.kgummo.washstaff.WebService.ws_DetalleServicioResult;
import mx.com.kgummo.washstaff.WebService.ws_ServicioRequest;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class HistorialActivity extends AppCompatActivity {

    ListView listView;
    adaServicioheader adapter;
    clsDetalleServicio servicio;
    clsServicioH oServicio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historial);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText("Historial de servicios");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listView = (ListView) findViewById(R.id.lstServicios);
        adapter = new adaServicioheader(this, clsGlobales.gblServiciosHeader.Servicios);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                oServicio = (clsServicioH) listView.getItemAtPosition(i);
                GetDetail gDetail = new GetDetail(oServicio.IdOrden);
                gDetail.execute();

            }
        });

        listView.setAdapter(adapter);
    }

    class GetDetail extends AsyncTask<Void,Void,Boolean>
    {
        private ProgressDialog pDialog;
        private final Integer idOrden;
        private String result;

        GetDetail(Integer IdOrden)
        {
            idOrden = IdOrden;
        }

        @Override
        protected void onPreExecute()
        {
            pDialog = new ProgressDialog(HistorialActivity.this);
            pDialog.setTitle("WashBer");
            pDialog.setMessage("Obteniendo servicio...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... params)
        {
            String url = clsGlobales.API_URL + clsGlobales.API_SERVICIODETALLE;
            OkHttpClient client = new OkHttpClient();
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            Gson h = new Gson();
            ws_ServicioRequest oRequest = new ws_ServicioRequest();

            try
            {
                oRequest.IdOrden = idOrden;
                String jsRequest = h.toJson(oRequest);
                RequestBody body = RequestBody.create(JSON, jsRequest);

                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();

                return true;
            }
            catch (Exception e) {
                e.printStackTrace();
                pDialog.dismiss();
                return false;

            }
        }

        protected void onPostExecute(final Boolean success)
        {
            try
            {
                ws_DetalleServicioResult oResult = new Gson().fromJson(String.valueOf(result),ws_DetalleServicioResult.class);

                if(oResult.Codigo == 101)
                {
                    MyDynamicToast.errorMessage(getApplication(), "Tuvimos un problema al obtener la información, intenta mas tarde.");
                }
                else if(oResult.Codigo == 100)
                {
                    servicio = new clsDetalleServicio();
                    servicio.PrendasOrden = oResult.PrendasOrden;
                    servicio.PrendasP = oResult.PrendasP;
                    servicio.Proveedor = oResult.Proveedor;
                    servicio.FechaCreacion = oResult.FechaCreacion;
                    servicio.FechaSolicitadaP = oResult.FechaSolicitadaP;
                    servicio.FechaProgramadaP = oResult.FechaProgramadaP;
                    servicio.FechaRealP = oResult.FechaRealP;
                    servicio.FechaSolicitadaD = oResult.FechaSolicitadaD;
                    servicio.FechaProgramadaD = oResult.FechaProgramadaD;
                    servicio.FechaRealD = oResult.FechaRealD;
                    servicio.Observaciones = oResult.Observaciones;
                    servicio.Latitud = oResult.Latitud;
                    servicio.Longitud = oResult.Longitud;
                    servicio.Referencia = oResult.Referencia;
                    servicio.Estatus = oResult.Estatus;
                    servicio.LogoProveedor = oResult.LogoProveedor;
                    DisplayDetail();
                }
                else
                {
                    MyDynamicToast.warningMessage(getApplication(), oResult.Mensaje);
                }

                pDialog.dismiss();
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }
    }

    public void DisplayDetail()
    {
        final Dialog dialog  = new Dialog(this);

        dialog.setContentView(R.layout.layout_modalservicio);

        MaterialFancyButton dismissButton = (MaterialFancyButton) dialog.findViewById(R.id.cmdCerrar);
        dismissButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        //ETIQUETAS DEL MODAL
        TextView texto;
        ImageView imgProveedor;

        texto = (TextView) dialog.findViewById(R.id.txtFolioModalServicio);
        texto.setText("Folio: " + oServicio.Folio);

        texto = (TextView) dialog.findViewById(R.id.txtEstatusServicio);
        texto.setText(servicio.Estatus);

        texto = (TextView) dialog.findViewById(R.id.txtCreacionServicio);
        texto.setText("Registrado: " + servicio.FechaCreacion);

        texto = (TextView) dialog.findViewById(R.id.txtSolicitudPModalServicio);
        texto.setText(servicio.FechaSolicitadaP);

        texto = (TextView) dialog.findViewById(R.id.txtSolicitudDModalServicio);
        texto.setText(servicio.FechaSolicitadaD);

        if(servicio.FechaProgramadaP.equals(""))
        {
            texto = (TextView) dialog.findViewById(R.id.txtProgramadoPModalServicio);
            texto.setText("PENDIENTE");
            texto = (TextView) dialog.findViewById(R.id.txtProgramadoDModalServicio);
            texto.setText("PENDIENTE");
        }
        else
        {
            texto = (TextView) dialog.findViewById(R.id.txtProgramadoPModalServicio);
            texto.setText(servicio.FechaProgramadaP);
            texto = (TextView) dialog.findViewById(R.id.txtProgramadoDModalServicio);
            if(servicio.FechaProgramadaD.equals(""))
            {
                texto.setText("PENDIENTE");
            }
            else
            {
                texto.setText(servicio.FechaProgramadaD);
            }
        }

        if(servicio.FechaRealP.equals(""))
        {
            texto = (TextView) dialog.findViewById(R.id.txtRealPModalServicio);
            texto.setText("PENDIENTE");
            texto = (TextView) dialog.findViewById(R.id.txtRealDModalServicio);
            texto.setText("PENDIENTE");
            texto = (TextView) dialog.findViewById(R.id.txtPrendasPModalServicio);
            texto.setText("");
        }
        else
        {
            texto = (TextView) dialog.findViewById(R.id.txtRealPModalServicio);
            texto.setText(servicio.FechaRealP);
            texto = (TextView) dialog.findViewById(R.id.txtRealDModalServicio);
            if(servicio.FechaRealD.equals(""))
            {
                texto.setText("PENDIENTE");
            }
            else
            {
                texto.setText(servicio.FechaRealD);
            }
            texto = (TextView) dialog.findViewById(R.id.txtPrendasPModalServicio);
            texto.setVisibility(TextView.VISIBLE);
            if(servicio.PrendasP == 1)
            {
                texto.setText("Una prenda en pick-up");
            }
            else
            {
                texto.setText(servicio.PrendasP.toString() + " prendas en pick-up");
            }
        }

        texto = (TextView) dialog.findViewById(R.id.txtPrendasModalServicio);
        if(servicio.PrendasOrden == 1)
        {
            texto.setText("Una prenda en el servicio");
        }
        else
        {
            texto.setText(servicio.PrendasOrden.toString() + " prendas en el servicio");
        }

        texto = (TextView) dialog.findViewById(R.id.txtProveedorModalServicio);
        texto.setText("Proveedor: " + servicio.Proveedor);

        imgProveedor = (ImageView) dialog.findViewById(R.id.imgLogoProveedor);
        if(servicio.LogoProveedor.equals(""))
        {
            imgProveedor.setImageResource(R.drawable.iconosmall);
        }
        else
        {
            Picasso.with(this).load(servicio.LogoProveedor).into(imgProveedor);
        }
        dialog.setTitle("Detalle");
        dialog.setCancelable(false);
        dialog.show();


    }
}
