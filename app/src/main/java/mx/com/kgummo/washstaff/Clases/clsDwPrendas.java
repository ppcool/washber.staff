package mx.com.kgummo.washstaff.Clases;

import me.panavtec.wizard.WizardPage;
import mx.com.kgummo.washstaff.DwPrendasFragment;

/**
 * Created by ppcoo on 10/02/2018.
 */

public class clsDwPrendas extends WizardPage<DwPrendasFragment> {

    @Override
    public DwPrendasFragment createFragment()
    {
        return new DwPrendasFragment();
    }
}
