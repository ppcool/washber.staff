package mx.com.kgummo.washstaff;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kofigyan.stateprogressbar.StateProgressBar;
import com.rilixtech.materialfancybutton.MaterialFancyButton;

import mx.com.kgummo.washstaff.Clases.clsGlobales;


/**
 * A simple {@link Fragment} subclass.
 */
public class PwPrendasFragment extends Fragment implements OnMapReadyCallback {

    private MapView mapView;
    private GoogleMap googleMap;
    Location myLocation;
    LocationManager locationManager;

    public PwPrendasFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        TextView lblEtiqueta;
        MaterialFancyButton goNextButton;
        MaterialFancyButton goBackButton;
        MaterialFancyButton cancelButton;

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pw_prendas, container, false);

        //TOOLBAR
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText("Detalle");

        //BARRA DE PROGRESO
        StateProgressBar stateProgressBar = (StateProgressBar) view.findViewById(R.id.pgrBar);
        stateProgressBar.setCurrentStateNumber(StateProgressBar.StateNumber.ONE);

        //INFORMACIÖN
        lblEtiqueta = (TextView) view.findViewById(R.id.lblFolio);
        lblEtiqueta.setText("Folio: " + clsGlobales.gblServicioHeader.Folio);

        lblEtiqueta = (TextView) view.findViewById(R.id.lblCliente);
        lblEtiqueta.setText(clsGlobales.gblServicioHeader.Cliente);

        lblEtiqueta = (TextView) view.findViewById(R.id.lblFechaSolicitada);
        lblEtiqueta.setText("Fecha solicitada: " + clsGlobales.gblDetalleServicio.FechaSolicitadaP.toString());

        lblEtiqueta = (TextView) view.findViewById(R.id.lblFechaProgramada);
        lblEtiqueta.setText("Fecha programada: " + clsGlobales.gblDetalleServicio.FechaProgramadaP.toString());


        lblEtiqueta = (TextView) view.findViewById(R.id.lblPrendasPiezas);
        lblEtiqueta.setText("Prendas: " + clsGlobales.gblServicioHeader.Prendas.toString() + ", piezas: "
                + clsGlobales.gblServicioHeader.Piezas.toString());

        lblEtiqueta = (TextView) view.findViewById(R.id.lblReferencia);
        lblEtiqueta.setText("Referencia: " + clsGlobales.gblDetalleServicio.Referencia.toString());

        //BOTONES
        goNextButton = (MaterialFancyButton) view.findViewById(R.id.goNextButton);
        goBackButton = (MaterialFancyButton) view.findViewById(R.id.goBackButton);
        cancelButton = (MaterialFancyButton) view.findViewById(R.id.cmdCancelar);
        goBackButton.setVisibility(View.GONE);

        cancelButton.setOnClickListener(new View.OnClickListener()
        {
            @Override public void onClick(View v)
            {
                Cancel();
            }
        });
        goNextButton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                NextStep();
            }
        });

        //MAPA
        mapView = (MapView) view.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);

        return  view;
    }

    public void  NextStep()
    {
        ((PickupWizardActivity) getActivity()).getWizard().navigateNext();
    }

    public void onResume() {

        super.onResume();

        mapView.onResume();

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        mapView.getMapAsync(this);
        prepareLocationUpdates();

        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        criteria.setCostAllowed(true);
        criteria.setPowerRequirement(Criteria.POWER_LOW);
        String provider = locationManager.getBestProvider(criteria, true);
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION},
                    2);
        } else
            locationManager.requestLocationUpdates(provider, 1 * 60 * 1000, 10, locationListener);
    }

    private void prepareLocationUpdates() {

        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        criteria.setCostAllowed(true);
        criteria.setPowerRequirement(Criteria.POWER_LOW);
        String provider = locationManager.getBestProvider(criteria, true);

        if (provider != null) {
            if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                    PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) ==
                            PackageManager.PERMISSION_GRANTED) {
                locationManager.requestLocationUpdates(provider, 1 * 60 * 1000, 10, locationListener);
            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION},
                        2);
            }
        }
    }

    android.location.LocationListener locationListener = new android.location.LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            if (googleMap != null) {
                if (location != null)
                {
                    googleMap.clear();
                    //LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                    LatLng latLng = new LatLng(clsGlobales.gblDetalleServicio.Latitud, clsGlobales.gblDetalleServicio.Longitud);
                    myLocation = location;

                    googleMap.addMarker(new MarkerOptions().position(latLng).title("Ubicación de Pick-Up"));

                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 19);
                    googleMap.animateCamera(cameraUpdate);

                }
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    @Override
    public void onMapReady(GoogleMap gMap)
    {
        googleMap = gMap;

        if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED) {
            googleMap.setMyLocationEnabled(true);
            googleMap.getUiSettings().setMyLocationButtonEnabled(true);

            // Create a criteria object to retrieve provider
            Criteria criteria = new Criteria();
            criteria.setAccuracy(Criteria.ACCURACY_FINE);
            criteria.setAltitudeRequired(false);
            criteria.setBearingRequired(false);
            criteria.setCostAllowed(true);
            criteria.setPowerRequirement(Criteria.POWER_LOW);
            // Get the name of the best provider
            String provider = locationManager.getBestProvider(criteria, true);

            // Ubicación actual
            myLocation = locationManager.getLastKnownLocation(provider);

            if (myLocation != null)
            {
                double latitude = clsGlobales.gblDetalleServicio.Latitud;
                double longitude = clsGlobales.gblDetalleServicio.Longitud;

                LatLng latLng = new LatLng(latitude, longitude);

                //Marcador de ubicación
                MarkerOptions marcador = new MarkerOptions();
                marcador.position(new LatLng(latitude, longitude));
                marcador.title("Ubicación de Pick-Up");
                marcador.draggable(true);
                googleMap.addMarker(marcador);



                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 2);
                googleMap.animateCamera(cameraUpdate);
            }


        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION},
                    1);
        }
    }

    public void  Cancel()
    {
        Intent intent = new Intent(getActivity(),PickupActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

}
