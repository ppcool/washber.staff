package mx.com.kgummo.washstaff;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.desai.vatsal.mydynamictoast.MyDynamicToast;
import com.kofigyan.stateprogressbar.StateProgressBar;
import com.rilixtech.materialfancybutton.MaterialFancyButton;

import java.io.ByteArrayOutputStream;
import java.io.File;

import mx.com.kgummo.washstaff.Adaptadores.adaPrendaItemp;
import mx.com.kgummo.washstaff.Clases.clsGlobales;
import mx.com.kgummo.washstaff.Clases.clsPrenda;


/**
 * A simple {@link Fragment} subclass.
 */
public class PwPictureprendasFragment extends Fragment {

    private adaPrendaItemp aPrendas;
    ListView lvPrendas;
    clsPrenda oPrenda;
    Uri file;
    private static final int CAMERA_REQUEST = 1888;
    String encodedImage = "";

    public PwPictureprendasFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pw_pictureprendas, container, false);

        //TOOLBAR
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText("Fotografía");

        //BARRA DE PROGRESO
        StateProgressBar stateProgressBar = (StateProgressBar) view.findViewById(R.id.pgrBar);
        stateProgressBar.setCurrentStateNumber(StateProgressBar.StateNumber.THREE);

        //BOTONES
        MaterialFancyButton goNextButton = (MaterialFancyButton) view.findViewById(R.id.goNextButton);
        goNextButton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                ValidateData();
            }
        });

        MaterialFancyButton goBackButton = (MaterialFancyButton) view.findViewById(R.id.goBackButton);
        goBackButton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                ((PickupWizardActivity) getActivity()).getWizard().navigatePrevious();
            }
        });

        MaterialFancyButton cancelButton = (MaterialFancyButton) view.findViewById(R.id.cmdCancelar);
        cancelButton.setOnClickListener(new View.OnClickListener()
        {
            @Override public void onClick(View v)
            {
                Cancel();
            }
        });

        //LISTA DE PRENDAS
        lvPrendas = (ListView) view.findViewById(R.id.lstPrendas);
        aPrendas = new adaPrendaItemp(getContext(), clsGlobales.gblPrendasP);
        lvPrendas.setAdapter(aPrendas);

        lvPrendas.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                oPrenda = clsGlobales.gblPrendasP.get(i);
                GetPicture();
            }
        });

        return view;
    }

    public void  Cancel()
    {
        Intent intent = new Intent(getActivity(),PickupActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    public void GetPicture()
    {
        try {

            //File photo = new File(Environment.getExternalStorageDirectory(), "Pic.jpg");
            File photo = new File(getContext().getExternalFilesDir("trackit"), "Pic.jpg");
            file = Uri.fromFile(photo);

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            Fragment frag = this;
            intent.putExtra(MediaStore.EXTRA_OUTPUT, file);
            frag.startActivityForResult(intent, CAMERA_REQUEST);
        }
        catch(Exception ex)
        {
            MyDynamicToast.errorMessage(getContext(), "No es posible acceder a tu cámara.");
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK)
        {
            try
            {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), file);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                final int maxSize = 960;
                int outWidth;
                int outHeight;
                int inWidth = bitmap.getWidth();
                int inHeight = bitmap.getHeight();
                if(inWidth > inHeight){
                    outWidth = maxSize;
                    outHeight = (inHeight * maxSize) / inWidth;
                } else {
                    outHeight = maxSize;
                    outWidth = (inWidth * maxSize) / inHeight;
                }

                Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, outWidth, outHeight, false);
                resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                byte[] byteArray = byteArrayOutputStream .toByteArray();
                encodedImage = Base64.encodeToString(byteArray, Base64.DEFAULT);
                oPrenda.Imagen = encodedImage;
                aPrendas = new adaPrendaItemp(getContext(), clsGlobales.gblPrendasP);
                lvPrendas.setAdapter(aPrendas);
            }
            catch (Exception ex) {
                MyDynamicToast.errorMessage(getContext(), "No es posible acceder a tu cámara.");
            }
        }
    }

    private void ValidateData()
    {
        boolean conFotos = true;

        for(clsPrenda oP : clsGlobales.gblPrendasP)
        {
            if(oP.Imagen == null)
            {
                conFotos = false;
                break;
            }
            else if (oP.Imagen.equals(""))
            {
                conFotos = false;
            }
        }

        if(!conFotos)
        {
            MyDynamicToast.errorMessage(getContext(), "Es necesario que especifiques la fotografía de todas las prendas.");
        }
        else
        {
            ((PickupWizardActivity) getActivity()).getWizard().navigateNext();
        }
    }

}
