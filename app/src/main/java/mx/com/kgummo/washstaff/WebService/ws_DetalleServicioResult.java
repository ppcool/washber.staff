package mx.com.kgummo.washstaff.WebService;

/**
 * Created by ppcoo on 02/02/2018.
 */

public class ws_DetalleServicioResult {
    public Integer Codigo;
    public String Mensaje;
    public Integer PrendasOrden;
    public Integer PrendasP;
    public String Proveedor;
    public String FechaCreacion;
    public String FechaSolicitadaP;
    public String FechaProgramadaP;
    public String FechaRealP;
    public  String FechaSolicitadaD;
    public String FechaProgramadaD;
    public String FechaRealD;
    public String Observaciones;
    public Float Latitud;
    public Float Longitud;
    public String Referencia;
    public String Estatus;
    public String LogoProveedor;
}
