package mx.com.kgummo.washstaff.WebService;

import java.util.ArrayList;

/**
 * Created by ppcoo on 08/02/2018.
 */

public class ws_SavePickRequest {
    public Integer IdOrden;
    public Integer IdRepartidor;
    public String Observaciones;
    public ArrayList<ws_PrendaOrden> Prendas;
}
