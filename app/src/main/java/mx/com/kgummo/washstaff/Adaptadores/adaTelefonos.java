package mx.com.kgummo.washstaff.Adaptadores;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import mx.com.kgummo.washstaff.Clases.clsTelefono;
import mx.com.kgummo.washstaff.R;

/**
 * Created by ppcoo on 01/03/2018.
 */

public class adaTelefonos extends ArrayAdapter<clsTelefono> {

    public adaTelefonos (Context context, ArrayList<clsTelefono> elementos)
    {
        super(context,0,elementos);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent)
    {
        clsTelefono elemento = getItem(position);
        String cadena;
        if(convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.layout_telefono,parent,false);
        }

        TextView text = (TextView) convertView.findViewById(R.id.txtTitulo);


        assert elemento != null;
        cadena = elemento.Titulo;
        text.setText(cadena);

        text = (TextView) convertView.findViewById(R.id.txtTelefono);
        cadena = elemento.Telefono;
        text.setText(cadena);

        return convertView;
    }
}
