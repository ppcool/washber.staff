package mx.com.kgummo.washstaff.Adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;

import mx.com.kgummo.washstaff.Clases.clsItemCheckList;
import mx.com.kgummo.washstaff.R;

/**
 * Created by ppcoo on 07/02/2018.
 */

public class adaCheckList extends BaseAdapter {

    private Context activity;
    private ArrayList<clsItemCheckList> data;
    private static LayoutInflater inflater = null;
    private View vi;
    private ViewHolder viewHolder;

    public adaCheckList(Context context, ArrayList<clsItemCheckList> items) {
        this.activity = context;
        this.data = items;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        vi = view;
        //Populate the Listview
        final int pos = position;
        clsItemCheckList items = data.get(pos);
        if(view == null) {
            vi = inflater.inflate(R.layout.layout_checklist, null);
            viewHolder = new ViewHolder();
            viewHolder.checkBox = (CheckBox) vi.findViewById(R.id.checkbox);
            viewHolder.name = (TextView) vi.findViewById(R.id.name);
            vi.setTag(viewHolder);
        }else
            viewHolder = (ViewHolder) view.getTag();
        viewHolder.name.setText(items.getCadena());
        if(items.isCheckox()){
            viewHolder.checkBox.setChecked(true);
        }
        else {
            viewHolder.checkBox.setChecked(false);
        }
        return vi;
    }

    public ArrayList<clsItemCheckList> getAllData(){
        return data;
    }

    public void setCheckBox(int position){
        //Update status of checkbox
        clsItemCheckList items = data.get(position);
        items.setCheckox(!items.isCheckox());
        notifyDataSetChanged();
    }

    public class ViewHolder{
        TextView name;
        CheckBox checkBox;
    }
}
