package mx.com.kgummo.washstaff.Clases;

/**
 * Created by ppcoo on 02/02/2018.
 */

public class clsDetalleServicio {
    public Integer PrendasOrden;
    public Integer PrendasP;
    public String Proveedor;
    public String FechaCreacion;
    public String FechaSolicitadaP;
    public String FechaProgramadaP;
    public String FechaRealP;
    public  String FechaSolicitadaD;
    public String FechaProgramadaD;
    public String FechaRealD;
    public String Observaciones;
    public Float Latitud;
    public Float Longitud;
    public String Referencia;
    public String Estatus;
    public String LogoProveedor;
}
