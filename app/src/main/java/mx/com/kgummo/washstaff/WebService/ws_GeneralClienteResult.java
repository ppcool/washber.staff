package mx.com.kgummo.washstaff.WebService;

import java.util.ArrayList;

/**
 * Created by ppcoo on 01/03/2018.
 */

public class ws_GeneralClienteResult {
    public Integer Codigo;
    public String Mensaje;
    public String Correo;
    public String FActivacion;
    public String FNacimiento;
    public String FRegistro;
    public String Fotografia;
    public String Nombre;
    public ArrayList<ws_TelefonoResult> Telefonos;
}
