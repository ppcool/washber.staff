package mx.com.kgummo.washstaff.Clases;

import me.panavtec.wizard.WizardPage;
import mx.com.kgummo.washstaff.DwConfirmacionFragment;

/**
 * Created by ppcoo on 10/02/2018.
 */

public class clsDwConfirmacion extends WizardPage<DwConfirmacionFragment> {

    @Override
    public DwConfirmacionFragment createFragment()
    {
        return new DwConfirmacionFragment();
    }
}
