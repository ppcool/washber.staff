package mx.com.kgummo.washstaff.Adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import mx.com.kgummo.washstaff.Clases.clsTipoIncidencia;
import mx.com.kgummo.washstaff.R;

/**
 * Created by ppcoo on 06/03/2018.
 */

public class adaTiposIncidencia extends BaseAdapter {

    Context context;
    ArrayList<clsTipoIncidencia> tipos;
    LayoutInflater inflter;

    public adaTiposIncidencia(Context applicationContext,ArrayList<clsTipoIncidencia> Tipos)
    {
        this.context = applicationContext;
        inflter = (LayoutInflater.from(applicationContext));
        this.tipos = Tipos;
    }

    @Override
    public int getCount() {return tipos.size();}

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {return 0;}

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        view = inflter.inflate(R.layout.layout_tipoincidenciaitem, null);
        TextView razonSocial = (TextView) view.findViewById(R.id.lblTipo);

        clsTipoIncidencia prov = tipos.get(i);
        razonSocial.setText(prov.Nombre);

        return view;
    }
}
