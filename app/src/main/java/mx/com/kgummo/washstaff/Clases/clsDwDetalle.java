package mx.com.kgummo.washstaff.Clases;

import me.panavtec.wizard.WizardPage;
import mx.com.kgummo.washstaff.DwDetalleFragment;

/**
 * Created by ppcoo on 10/02/2018.
 */

public class clsDwDetalle extends WizardPage<DwDetalleFragment> {

    @Override
    public DwDetalleFragment createFragment()
    {
        return new DwDetalleFragment();
    }
}
