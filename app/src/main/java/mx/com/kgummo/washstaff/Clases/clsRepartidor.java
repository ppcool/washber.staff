package mx.com.kgummo.washstaff.Clases;

/**
 * Created by ppcoo on 02/02/2018.
 */

public class clsRepartidor {
    public Integer IdRepartidor;
    public String Nombre;
    public String CorreoElectronico;
    public String Fotografia;
    public String Proveedor;
    public String LogoProveedor;
    public Integer PendienteDrop;
    public Integer PendientePick;
    public Integer DropOff;
    public Integer PickUp;
    public Float MontoMinimoServicio;
}
