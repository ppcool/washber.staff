package mx.com.kgummo.washstaff;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.desai.vatsal.mydynamictoast.MyDynamicToast;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import mx.com.kgummo.washstaff.Clases.clsGlobales;
import mx.com.kgummo.washstaff.Clases.clsServicioH;
import mx.com.kgummo.washstaff.Clases.clsServiciosH;
import mx.com.kgummo.washstaff.WebService.ws_GenericIntRequest;
import mx.com.kgummo.washstaff.WebService.ws_ServicioHResult;
import mx.com.kgummo.washstaff.WebService.ws_ServiciosHResult;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class PrincipalActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        //ACTIVA BARRA
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // PINTA BOTON PARA ABRIR MENU FLOTANTE
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        //VALORES DE USUARIO
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View header=navigationView.getHeaderView(0);
        TextView lblNombre = (TextView) header.findViewById(R.id.profile_name);
        ImageView imgUsuario = (ImageView) header.findViewById(R.id.profile_pic);

        lblNombre.setText(clsGlobales.gblRepartidor.Nombre);
        try {
            Picasso.with(this).load(clsGlobales.gblRepartidor.Fotografia).into(imgUsuario);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        //ASIGNACION DE FRAGENTO PRINCIPAL
        if(savedInstanceState == null)
        {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_main, PrincipalFragment.newInstance())
                    .commit();
        }

        //MENU
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        //CIERRE DE SESION
        if(id == R.id.nav_cerrarsesion)
        {
            clsGlobales.gblRepartidor = null;
            Intent i = new Intent(this,MainActivity.class);
            startActivity(i);
            finish();
        }
        else if(id == R.id.nav_historial)
        {
            getServicios gSer = new getServicios();
            gSer.execute();
        }
        else if(id == R.id.nav_pickup)
        {
            getServiciosPP gSer = new getServiciosPP();
            gSer.execute();
        }
        else if(id == R.id.nav_drop)
        {
            getServiciosPD gSer = new getServiciosPD();
            gSer.execute();
        }

        return true;
    }

    class getServicios extends AsyncTask<Void,Void,Boolean>
    {
        private ProgressDialog pDialog;
        private String result;

        getServicios(){}

        @Override
        protected void onPreExecute()
        {
            pDialog = new ProgressDialog(PrincipalActivity.this);
            pDialog.setTitle("WashBer");
            pDialog.setMessage("Obteniendo servicios...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... params)
        {
            String url = clsGlobales.API_URL + clsGlobales.API_SERVICIOSR;
            OkHttpClient client = new OkHttpClient();
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            Gson h = new Gson();
            ws_GenericIntRequest oRequest = new ws_GenericIntRequest();

            try
            {
                oRequest.Valor = clsGlobales.gblRepartidor.IdRepartidor;
                String jsRequest = h.toJson(oRequest);
                RequestBody body = RequestBody.create(JSON, jsRequest);

                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                return true;
            }
            catch (Exception e) {
                e.printStackTrace();
                pDialog.dismiss();
                return false;
            }
        }

        protected void onPostExecute(final Boolean success)
        {
            clsServicioH ser;

            try
            {
                ws_ServiciosHResult oResult  = new Gson().fromJson(String.valueOf(result),ws_ServiciosHResult.class);
                if(success)
                {
                    if(oResult.Codigo == 100)
                    {
                        if(oResult.Servicios.size() == 0)
                        {
                            pDialog.dismiss();
                            MyDynamicToast.warningMessage(PrincipalActivity.this, "Aún no has atendido servicios.");
                        }
                        else
                        {
                            clsGlobales.gblServiciosHeader = new clsServiciosH();
                            clsGlobales.gblServiciosHeader.Servicios = new ArrayList<clsServicioH>();
                            //cl_Global.gblServiciosHeader.Servicios = oResult.Servicios;
                            for(int i= 0; i < oResult.Servicios.size(); i++)
                            {
                                ws_ServicioHResult r = oResult.Servicios.get(i);
                                ser = new clsServicioH();
                                ser.Estatus = r.Estatus;
                                ser.Fecha = r.Fecha;
                                ser.Folio = r.Folio;
                                ser.IdOrden = r.IdOrden;
                                clsGlobales.gblServiciosHeader.Servicios.add(ser);
                            }
                            pDialog.dismiss();
                            DisplayHistory();
                        }
                    }
                    else if(oResult.Codigo == 101)
                    {
                        MyDynamicToast.errorMessage(PrincipalActivity.this, "Tenemos un problema con la información, reintenta mas tarde.");
                        pDialog.dismiss();
                    }
                    else
                    {
                        MyDynamicToast.warningMessage(PrincipalActivity.this, oResult.Mensaje);
                        pDialog.dismiss();
                    }
                }
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                pDialog.dismiss();
            }
        }
    }

    public void DisplayHistory()
    {
        Intent i = new Intent(PrincipalActivity.this,HistorialActivity.class);
        startActivity(i);
    }

    class getServiciosPP extends AsyncTask<Void,Void,Boolean>
    {
        private ProgressDialog pDialog;
        private String result;

        getServiciosPP(){}

        @Override
        protected void onPreExecute()
        {
            pDialog = new ProgressDialog(PrincipalActivity.this);
            pDialog.setTitle("WashBer");
            pDialog.setMessage("Obteniendo servicios...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... params)
        {
            String url = clsGlobales.API_URL + clsGlobales.API_SERVICIOSPPR;
            OkHttpClient client = new OkHttpClient();
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            Gson h = new Gson();
            ws_GenericIntRequest oRequest = new ws_GenericIntRequest();

            try
            {
                oRequest.Valor = clsGlobales.gblRepartidor.IdRepartidor;
                String jsRequest = h.toJson(oRequest);
                RequestBody body = RequestBody.create(JSON, jsRequest);

                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                return true;
            }
            catch (Exception e) {
                e.printStackTrace();
                pDialog.dismiss();
                return false;
            }
        }

        protected void onPostExecute(final Boolean success)
        {
            clsServicioH ser;

            try
            {
                ws_ServiciosHResult oResult  = new Gson().fromJson(String.valueOf(result),ws_ServiciosHResult.class);
                if(success)
                {
                    if(oResult.Codigo == 100)
                    {
                        if(oResult.Servicios.size() == 0)
                        {
                            pDialog.dismiss();
                            MyDynamicToast.warningMessage(PrincipalActivity.this, "No tienes servicios pendientes de pick-up.");
                        }
                        else
                        {
                            clsGlobales.gblServiciosHeader = new clsServiciosH();
                            clsGlobales.gblServiciosHeader.Servicios = new ArrayList<clsServicioH>();
                            //cl_Global.gblServiciosHeader.Servicios = oResult.Servicios;
                            for(int i= 0; i < oResult.Servicios.size(); i++)
                            {
                                ws_ServicioHResult r = oResult.Servicios.get(i);
                                ser = new clsServicioH();
                                ser.Estatus = r.Estatus;
                                ser.Fecha = r.Fecha;
                                ser.Folio = r.Folio;
                                ser.IdOrden = r.IdOrden;
                                ser.Cliente = r.Cliente;
                                ser.Piezas = r.Piezas;
                                ser.Prendas = r.Prendas;
                                clsGlobales.gblServiciosHeader.Servicios.add(ser);
                            }
                            pDialog.dismiss();
                            DisplayPickUp();
                        }
                    }
                    else if(oResult.Codigo == 101)
                    {
                        MyDynamicToast.errorMessage(PrincipalActivity.this, "Tenemos un problema con la información, reintenta mas tarde.");
                        pDialog.dismiss();
                    }
                    else
                    {
                        MyDynamicToast.warningMessage(PrincipalActivity.this, oResult.Mensaje);
                        pDialog.dismiss();
                    }
                }
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                pDialog.dismiss();
            }
        }
    }

    public void DisplayPickUp()
    {

        Intent i = new Intent(PrincipalActivity.this,PickupActivity.class);
        startActivity(i);

    }

    class getServiciosPD extends AsyncTask<Void,Void,Boolean>
    {
        private ProgressDialog pDialog;
        private String result;

        getServiciosPD(){}

        @Override
        protected void onPreExecute()
        {
            pDialog = new ProgressDialog(PrincipalActivity.this);
            pDialog.setTitle("WashBer");
            pDialog.setMessage("Obteniendo servicios...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... params)
        {
            String url = clsGlobales.API_URL + clsGlobales.API_SERVICIOSPDR;
            OkHttpClient client = new OkHttpClient();
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            Gson h = new Gson();
            ws_GenericIntRequest oRequest = new ws_GenericIntRequest();

            try
            {
                oRequest.Valor = clsGlobales.gblRepartidor.IdRepartidor;
                String jsRequest = h.toJson(oRequest);
                RequestBody body = RequestBody.create(JSON, jsRequest);

                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                return true;
            }
            catch (Exception e) {
                e.printStackTrace();
                pDialog.dismiss();
                return false;
            }
        }

        protected void onPostExecute(final Boolean success)
        {
            clsServicioH ser;

            try
            {
                ws_ServiciosHResult oResult  = new Gson().fromJson(String.valueOf(result),ws_ServiciosHResult.class);
                if(success)
                {
                    if(oResult.Codigo == 100)
                    {
                        if(oResult.Servicios.size() == 0)
                        {
                            pDialog.dismiss();
                            MyDynamicToast.warningMessage(PrincipalActivity.this, "No tienes servicios pendientes de drop-off.");
                        }
                        else
                        {
                            clsGlobales.gblServiciosHeader = new clsServiciosH();
                            clsGlobales.gblServiciosHeader.Servicios = new ArrayList<clsServicioH>();

                            for(int i= 0; i < oResult.Servicios.size(); i++)
                            {
                                ws_ServicioHResult r = oResult.Servicios.get(i);
                                ser = new clsServicioH();
                                ser.Estatus = r.Estatus;
                                ser.Fecha = r.Fecha;
                                ser.Folio = r.Folio;
                                ser.IdOrden = r.IdOrden;
                                ser.Cliente = r.Cliente;
                                ser.Piezas = r.Piezas;
                                ser.Prendas = r.Prendas;
                                clsGlobales.gblServiciosHeader.Servicios.add(ser);
                            }
                            pDialog.dismiss();
                            DisplayDropOff();
                        }
                    }
                    else if(oResult.Codigo == 101)
                    {
                        MyDynamicToast.errorMessage(PrincipalActivity.this, "Tenemos un problema con la información, reintenta mas tarde.");
                        pDialog.dismiss();
                    }
                    else
                    {
                        MyDynamicToast.warningMessage(PrincipalActivity.this, oResult.Mensaje);
                        pDialog.dismiss();
                    }
                }
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                pDialog.dismiss();
            }
        }
    }

    public void DisplayDropOff()
    {
        Intent i = new Intent(PrincipalActivity.this,DropActivity.class);
        startActivity(i);
    }


}
