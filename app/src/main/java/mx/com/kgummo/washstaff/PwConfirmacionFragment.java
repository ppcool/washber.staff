package mx.com.kgummo.washstaff;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.desai.vatsal.mydynamictoast.MyDynamicToast;
import com.google.gson.Gson;
import com.kofigyan.stateprogressbar.StateProgressBar;
import com.rilixtech.materialfancybutton.MaterialFancyButton;

import java.util.ArrayList;

import mx.com.kgummo.washstaff.Adaptadores.adaPrendaItemp;
import mx.com.kgummo.washstaff.Clases.clsGlobales;
import mx.com.kgummo.washstaff.Clases.clsPrenda;
import mx.com.kgummo.washstaff.WebService.ws_GenericResult;
import mx.com.kgummo.washstaff.WebService.ws_PrendaOrden;
import mx.com.kgummo.washstaff.WebService.ws_SavePickRequest;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class PwConfirmacionFragment extends Fragment {

    clsPrenda oPrenda;

    public PwConfirmacionFragment() {}


    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        TextView lblEtiqueta;
        Button goNextButton;
        MaterialFancyButton goBackButton;
        MaterialFancyButton cancelButton;
        MaterialFancyButton confirmButton;
        ListView lvPrendas;
        adaPrendaItemp aPrendas;

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pw_confirmacion, container, false);

        //TOOLBAR
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText("Confirmación");

        //BARRA DE PROGRESO
        StateProgressBar stateProgressBar = (StateProgressBar) view.findViewById(R.id.pgrBar);
        stateProgressBar.setCurrentStateNumber(StateProgressBar.StateNumber.FOUR);

        //INFORMACIÖN
        lblEtiqueta = (TextView) view.findViewById(R.id.lblFolio);
        lblEtiqueta.setText("Folio: " + clsGlobales.gblServicioHeader.Folio);

        lblEtiqueta = (TextView) view.findViewById(R.id.lblCliente);
        lblEtiqueta.setText(clsGlobales.gblServicioHeader.Cliente);

        lblEtiqueta = (TextView) view.findViewById(R.id.lblFechaSolicitada);
        lblEtiqueta.setText("Fecha solicitada: " + clsGlobales.gblDetalleServicio.FechaSolicitadaP);

        lblEtiqueta = (TextView) view.findViewById(R.id.lblFechaProgramada);
        lblEtiqueta.setText("Fecha programada: " + clsGlobales.gblDetalleServicio.FechaProgramadaP);


        lblEtiqueta = (TextView) view.findViewById(R.id.lblPrendasPiezas);
        lblEtiqueta.setText("Prendas: " + clsGlobales.gblServicioHeader.Prendas.toString() + ", piezas: "
                + clsGlobales.gblServicioHeader.Piezas.toString());

        lblEtiqueta = (TextView) view.findViewById(R.id.lblReferencia);
        lblEtiqueta.setText("Referencia: " + clsGlobales.gblDetalleServicio.Referencia);

        //BOTONES
        goNextButton = (Button) view.findViewById(R.id.goNextButton);
        goBackButton = (MaterialFancyButton) view.findViewById(R.id.goBackButton);

        goNextButton.setVisibility(View.GONE);

        goBackButton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                ((PickupWizardActivity) getActivity()).getWizard().navigatePrevious();
            }
        });

        cancelButton = (MaterialFancyButton) view.findViewById(R.id.cmdCancelar);
        cancelButton.setOnClickListener(new View.OnClickListener()
        {
            @Override public void onClick(View v)
            {
                Cancel();
            }
        });

        confirmButton = (MaterialFancyButton) view.findViewById(R.id.cmdConfirmar);
        confirmButton.setOnClickListener(new View.OnClickListener()
        {
            @Override public void onClick(View v)
            {
                savePickUp saveP = new savePickUp(clsGlobales.gblServicioHeader.IdOrden
                        ,clsGlobales.gblRepartidor.IdRepartidor,clsGlobales.gblPrendasP);

                saveP.execute();
            }
        });

        //LISTA DE PRENDAS
        lvPrendas = (ListView) view.findViewById(R.id.lstPrendas);
        aPrendas = new adaPrendaItemp(getContext(), clsGlobales.gblPrendasP);
        lvPrendas.setAdapter(aPrendas);

        lvPrendas.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                oPrenda = clsGlobales.gblPrendasP.get(i);
                GetPicture();
            }
        });

        return view;
    }

    public void  Cancel()
    {
        Intent intent = new Intent(getActivity(),PickupActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    public void GetPicture()
    {
        final Dialog dialog  = new Dialog(getActivity());
        ImageView imgFoto;

        dialog.setContentView(R.layout.layout_modalpicture);
        dialog.setTitle("Fotografía de prenda");
        imgFoto = (ImageView) dialog.findViewById(R.id.imgImagen);

        byte[] decodedString = Base64.decode(oPrenda.Imagen,Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        imgFoto.setImageBitmap(decodedByte);

        //EVENTO DE BOTÓN DE CERRAR
        MaterialFancyButton Cancelar = (MaterialFancyButton) dialog.findViewById(R.id.cmdCerrar);
        Cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        assert window != null;
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    @SuppressLint("StaticFieldLeak")
    class savePickUp extends AsyncTask<Void,Void, Boolean>
    {
        private ProgressDialog pDialog;
        private String result;
        private Integer idOrden;
        private Integer idRepartidor;
        private ArrayList<clsPrenda> prendas;

        savePickUp(Integer IdOrden, Integer IdRepartidor,ArrayList<clsPrenda> Prendas )
        {
            this.idOrden = IdOrden;
            this.idRepartidor = IdRepartidor;
            this.prendas = Prendas;
        }

        @Override
        protected void onPreExecute()
        {
            pDialog = new ProgressDialog(getActivity());
            pDialog.setTitle("WashBer");
            pDialog.setMessage("Procesando...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... params)
        {
            String url = clsGlobales.API_URL + clsGlobales.API_SAVEP;
            OkHttpClient client = new OkHttpClient();
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            Gson h = new Gson();
            ws_PrendaOrden oPre;
            ws_SavePickRequest oRequest = new ws_SavePickRequest();
            try
            {
                oRequest.IdOrden = this.idOrden;
                oRequest.IdRepartidor = this.idRepartidor;
                oRequest.Observaciones = "";
                oRequest.Prendas = new ArrayList<ws_PrendaOrden>();
                for(clsPrenda oP : this.prendas)
                {
                    oPre = new ws_PrendaOrden();
                    oPre.IdPrendaOrden = oP.IdPrendaOrden;
                    oPre.Fotografia = oP.Imagen;
                    oPre.Observaciones = "";
                    oRequest.Prendas.add(oPre);
                }

                String jsRequest = h.toJson(oRequest);
                RequestBody body = RequestBody.create(JSON, jsRequest);
                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                return true;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                pDialog.dismiss();
                return false;
            }
        }

        protected void onPostExecute(final Boolean success)
        {
            try
            {
                ws_GenericResult oResult = new Gson().fromJson(String.valueOf(result),ws_GenericResult.class);
                if(success)
                {
                    if(oResult.Codigo == 100)
                    {
                        MyDynamicToast.successMessage(getActivity(), "Pick-up registrado.");
                        pDialog.dismiss();
                        closeWizard();
                    }
                    else if (oResult.Codigo == 102)
                    {
                        MyDynamicToast.informationMessage(getActivity(), oResult.Mensaje);
                        pDialog.dismiss();
                    }
                    else
                    {
                        MyDynamicToast.errorMessage(getActivity(), "Tuvimos un problema al registrar tu servicio, intenta nuevamente.");
                        pDialog.dismiss();
                    }
                }
            }
            catch (Exception ex)
            {
                MyDynamicToast.errorMessage(getActivity(), "Tuvimos un problema al registrar el Pick-Up, intenta nuevamente.");
            }
        }
    }

    private void closeWizard()
    {
        Intent i = new Intent(getContext(),PrincipalActivity.class);
        startActivity(i);
        getActivity().finish();
    }

}
