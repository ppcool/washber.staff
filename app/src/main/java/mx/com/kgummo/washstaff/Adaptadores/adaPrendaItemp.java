package mx.com.kgummo.washstaff.Adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.ArrayList;

import mx.com.kgummo.washstaff.Clases.clsPrenda;
import mx.com.kgummo.washstaff.R;

/**
 * Created by ppcoo on 08/02/2018.
 */

public class adaPrendaItemp extends ArrayAdapter<clsPrenda> {

    public adaPrendaItemp (Context context, ArrayList<clsPrenda> elementos)
    {
        super(context,0,elementos);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        clsPrenda oPrenda = getItem(position);
        NumberFormat format = NumberFormat.getCurrencyInstance();

        if(convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.layout_prendaitemp,parent,false);
        }

        TextView Categoria = (TextView) convertView.findViewById(R.id.lblCategoria);
        if(oPrenda.Imagen.trim().equals("")) {
            Categoria.setText(oPrenda.Categoria + " (SIN FOTOGRAFIA)");
        }
        else
        {
            Categoria.setText(oPrenda.Categoria);
        }

        TextView Subcategoria = (TextView) convertView.findViewById(R.id.lblSubcategoria);
        Subcategoria.setText(oPrenda.Subcategoria);

        TextView Piezas = (TextView) convertView.findViewById(R.id.lblPiezas);
        Piezas.setText("Piezas " + oPrenda.Piezas.toString());

        TextView Precio = (TextView) convertView.findViewById(R.id.lblPrecio);
        Precio.setText("Precio por pieza " + format.format(oPrenda.Precio));

        return convertView;
    }
}
