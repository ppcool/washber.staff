package mx.com.kgummo.washstaff;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.desai.vatsal.mydynamictoast.MyDynamicToast;
import com.kofigyan.stateprogressbar.StateProgressBar;
import com.rilixtech.materialfancybutton.MaterialFancyButton;

import java.util.ArrayList;

import mx.com.kgummo.washstaff.Adaptadores.adaCheckList;
import mx.com.kgummo.washstaff.Clases.clsGlobales;
import mx.com.kgummo.washstaff.Clases.clsItemCheckList;
import mx.com.kgummo.washstaff.Clases.clsPrenda;


/**
 * A simple {@link Fragment} subclass.
 */
public class PwCheckprendas extends Fragment {

    private clsItemCheckList itemHandler;
    private adaCheckList adapter;
    private ListView listView;
    private ArrayList<clsItemCheckList> itemList;

    public PwCheckprendas() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        MaterialFancyButton goNextButton;
        MaterialFancyButton goBackButton;
        MaterialFancyButton cancelButton;

        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_pw_checkprendas, container, false);

        //TOOLBAR
        Toolbar toolbar = (Toolbar) v.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText("Prendas");

        //BARRA DE PROGRESO
        StateProgressBar stateProgressBar = (StateProgressBar) v.findViewById(R.id.pgrBar);
        stateProgressBar.setCurrentStateNumber(StateProgressBar.StateNumber.TWO);

        //BOTONES
        goNextButton = (MaterialFancyButton) v.findViewById(R.id.goNextButton);
        goBackButton = (MaterialFancyButton) v.findViewById(R.id.goBackButton);
        cancelButton = (MaterialFancyButton) v.findViewById(R.id.cmdCancelar);

        cancelButton.setOnClickListener(new View.OnClickListener()
        {
            @Override public void onClick(View v)
            {
                Cancel();
            }
        });

        goBackButton.setOnClickListener(new View.OnClickListener()
        {
            @Override public void onClick(View v)
            {
                ((PickupWizardActivity) getActivity()).getWizard().navigatePrevious();
            }
        });

        goNextButton.setOnClickListener(new View.OnClickListener()
        {
            @Override public void onClick(View v)
            {
                ValidateData();
            }
        });

        listView = (ListView) v.findViewById(R.id.listview);
        setWidget();

        return v;
    }

    public void setWidget()
    {
        itemList = new ArrayList<clsItemCheckList>();
        String cadena;

        for(Integer i = 0; i < clsGlobales.gblPrendasS.Prendas.size(); i++)
        {
            clsPrenda oPrenda = clsGlobales.gblPrendasS.Prendas.get(i);
            cadena = oPrenda.Categoria + ", " + oPrenda.Subcategoria + ", piezas: " + oPrenda.Piezas.toString();
            itemList.add(new clsItemCheckList (cadena, oPrenda.IdPrendaOrden,false));
        }

        adapter = new adaCheckList(getContext(),itemList);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView adapterView, View view, int position, long l) {
                //Item Selected from list
                adapter.setCheckBox(position);

            }
        });
    }

    public void  Cancel()
    {
        Intent intent = new Intent(getActivity(),PickupActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    public void ValidateData()
    {
        boolean seleccion = false;
        clsGlobales.gblPrendasP = new ArrayList<clsPrenda>();
        clsPrenda oPrenda;

        for(clsItemCheckList item: adapter.getAllData() )
        {
            if(item.isCheckox())
            {
                for(clsPrenda prenda : clsGlobales.gblPrendasS.Prendas)
                {
                    if(prenda.IdPrendaOrden == item.getId())
                    {
                        clsGlobales.gblPrendasP.add(prenda);
                        break;
                    }
                }
            }
        }

        if(clsGlobales.gblPrendasP.size() == 0)
        {
            MyDynamicToast.errorMessage(getContext(), "Selecciona al menos una prenda de Pick-Up.");
            return;
        }
        else
        {
            ((PickupWizardActivity) getActivity()).getWizard().navigateNext();
        }
    }
}
