package mx.com.kgummo.washstaff.WebService;

/**
 * Created by ppcoo on 02/02/2018.
 */

public class ws_RepartidorResult {
    public Integer Codigo;
    public String CorreoElectronico;
    public String Fotografia;
    public String Mensaje;
    public String Nombre;
    public String Proveedor;
    public Integer IdRepartidor;
    public String LogoProveedor;
    public Integer PendienteDrop;
    public Integer PendientePick;
    public Integer DropOff;
    public Integer PickUp;
    public Float MontoMinimoServicio;
}
