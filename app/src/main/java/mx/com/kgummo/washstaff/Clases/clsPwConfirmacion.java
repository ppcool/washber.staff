package mx.com.kgummo.washstaff.Clases;

import me.panavtec.wizard.WizardPage;
import mx.com.kgummo.washstaff.PwConfirmacionFragment;

/**
 * Created by ppcoo on 07/02/2018.
 */

public class clsPwConfirmacion extends WizardPage<PwConfirmacionFragment> {

    @Override
    public PwConfirmacionFragment createFragment()
    {
        return new PwConfirmacionFragment();
    }
}
