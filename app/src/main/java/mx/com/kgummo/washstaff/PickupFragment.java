package mx.com.kgummo.washstaff;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.desai.vatsal.mydynamictoast.MyDynamicToast;
import com.google.gson.Gson;

import java.util.ArrayList;

import mx.com.kgummo.washstaff.Adaptadores.adaServiciosPick;
import mx.com.kgummo.washstaff.Clases.clsDetalleServicio;
import mx.com.kgummo.washstaff.Clases.clsGlobales;
import mx.com.kgummo.washstaff.Clases.clsPrenda;
import mx.com.kgummo.washstaff.Clases.clsPrendas;
import mx.com.kgummo.washstaff.Clases.clsServicioH;
import mx.com.kgummo.washstaff.WebService.ws_DetalleServicioResult;
import mx.com.kgummo.washstaff.WebService.ws_GenericIntRequest;
import mx.com.kgummo.washstaff.WebService.ws_PrendaResult;
import mx.com.kgummo.washstaff.WebService.ws_PrendasResult;
import mx.com.kgummo.washstaff.WebService.ws_ServicioRequest;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class PickupFragment extends Fragment {

    ListView listView;
    adaServiciosPick adapter;
    clsServicioH oServicio;
    clsDetalleServicio servicio;
    ProgressDialog pDialog;

    public PickupFragment() {}

    public static PickupFragment newInstance()
    {
        return new PickupFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_pickup, container, false);

        listView = (ListView) v.findViewById(R.id.lstServicios);
        adapter = new adaServiciosPick(getContext(), clsGlobales.gblServiciosHeader.Servicios);

        //Evento click de la lista
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                oServicio = (clsServicioH) listView.getItemAtPosition(i);
                clsGlobales.gblServicioHeader = oServicio;
                GetDetail gDetail = new GetDetail(oServicio.IdOrden);
                gDetail.execute();
            }
        });

        listView.setAdapter(adapter);
        return v;
    }

    class GetDetail extends AsyncTask<Void,Void,Boolean>
    {
        private final Integer idOrden;
        private String result;

        GetDetail(Integer IdOrden)
        {
            idOrden = IdOrden;
        }

        @Override
        protected void onPreExecute()
        {
            pDialog = new ProgressDialog(getContext());
            pDialog.setTitle("WashBer");
            pDialog.setMessage("Obteniendo servicio...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... params)
        {
            String url = clsGlobales.API_URL + clsGlobales.API_SERVICIODETALLE;
            OkHttpClient client = new OkHttpClient();
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            Gson h = new Gson();
            ws_ServicioRequest oRequest = new ws_ServicioRequest();

            try
            {
                oRequest.IdOrden = idOrden;
                String jsRequest = h.toJson(oRequest);
                RequestBody body = RequestBody.create(JSON, jsRequest);

                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();

                return true;
            }
            catch (Exception e) {
                e.printStackTrace();
                pDialog.dismiss();
                return false;

            }
        }

        protected void onPostExecute(final Boolean success)
        {
            try
            {
                ws_DetalleServicioResult oResult = new Gson().fromJson(String.valueOf(result),ws_DetalleServicioResult.class);

                if(oResult.Codigo == 101)
                {
                    MyDynamicToast.errorMessage(getContext(), "Tuvimos un problema al obtener la información, intenta mas tarde.");
                }
                else if(oResult.Codigo == 100)
                {
                    servicio = new clsDetalleServicio();
                    servicio.PrendasOrden = oResult.PrendasOrden;
                    servicio.PrendasP = oResult.PrendasP;
                    servicio.Proveedor = oResult.Proveedor;
                    servicio.FechaCreacion = oResult.FechaCreacion;
                    servicio.FechaSolicitadaP = oResult.FechaSolicitadaP;
                    servicio.FechaProgramadaP = oResult.FechaProgramadaP;
                    servicio.FechaRealP = oResult.FechaRealP;
                    servicio.FechaSolicitadaD = oResult.FechaSolicitadaD;
                    servicio.FechaProgramadaD = oResult.FechaProgramadaD;
                    servicio.FechaRealD = oResult.FechaRealD;
                    servicio.Observaciones = oResult.Observaciones;
                    servicio.Latitud = oResult.Latitud;
                    servicio.Longitud = oResult.Longitud;
                    servicio.Referencia = oResult.Referencia;
                    servicio.Estatus = oResult.Estatus;
                    servicio.LogoProveedor = oResult.LogoProveedor;
                    GetPrendas();
                }
                else
                {
                    MyDynamicToast.warningMessage(getContext(), oResult.Mensaje);
                }

                //pDialog.dismiss();
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }
    }

    public void GetPrendas()
    {
        clsGlobales.gblDetalleServicio = servicio;
        clGetPrendas clGP = new clGetPrendas();
        clGP.execute();
    }

    class clGetPrendas extends AsyncTask<Void,Void,Boolean>
    {

        private String result;

        clGetPrendas(){}

        @Override
        protected void onPreExecute()
        {
            pDialog.setMessage("Obteniendo prendas...");
        }

        @Override
        protected Boolean doInBackground(Void... params)
        {
            String url = clsGlobales.API_URL + clsGlobales.API_PRENDASS;
            OkHttpClient client = new OkHttpClient();
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            Gson h = new Gson();
            ws_GenericIntRequest oRequest = new ws_GenericIntRequest();

            try
            {
                oRequest.Valor = oServicio.IdOrden;
                String jsRequest = h.toJson(oRequest);
                RequestBody body = RequestBody.create(JSON, jsRequest);

                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();

                return true;
            }
            catch (Exception e) {
                e.printStackTrace();
                pDialog.dismiss();
                return false;
            }
        }

        protected void onPostExecute(final Boolean success)
        {
            ws_PrendaResult oP;
            try
            {
                ws_PrendasResult oResult = new Gson().fromJson(String.valueOf(result),ws_PrendasResult.class);

                if(oResult.Codigo == 101)
                {
                    MyDynamicToast.errorMessage(getContext(), "Tuvimos un problema al obtener la información, intenta mas tarde.");
                }
                else if(oResult.Codigo == 100)
                {
                    clsGlobales.gblPrendasS = new clsPrendas();
                    clsGlobales.gblPrendasS.Prendas = new ArrayList<clsPrenda>();
                    clsPrenda oPrenda;
                    for(Integer i = 0; i < oResult.Prendas.size(); i++)
                    {
                        oPrenda = new clsPrenda();
                        oP = oResult.Prendas.get(i);
                        oPrenda.IdSubcategoria = oP.IdSubcategoria;
                        oPrenda.Precio = oP.Precio;
                        oPrenda.Piezas = oP.Piezas;
                        oPrenda.Imagen = "";
                        oPrenda.Categoria = oP.Categoria;
                        oPrenda.Subcategoria = oP.Subcategoria;
                        oPrenda.IdPrendaOrden = oP.IdPrendaOrden;

                        clsGlobales.gblPrendasS.Prendas.add(oPrenda);
                    }

                    pDialog.dismiss();
                    GotoWizard();
                }
                else
                {
                    MyDynamicToast.warningMessage(getContext(), oResult.Mensaje);
                }

                pDialog.dismiss();
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }
    }

    public void GotoWizard()
    {
        Intent i = new Intent(getContext(),PickupWizardActivity.class);
        startActivity(i);
    }
}
