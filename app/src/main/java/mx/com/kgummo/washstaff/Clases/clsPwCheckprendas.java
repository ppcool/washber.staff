package mx.com.kgummo.washstaff.Clases;

import me.panavtec.wizard.WizardPage;
import mx.com.kgummo.washstaff.PwCheckprendas;

/**
 * Created by ppcoo on 07/02/2018.
 */

public class clsPwCheckprendas extends WizardPage<PwCheckprendas> {

    @Override
    public PwCheckprendas createFragment()
    {
        return new PwCheckprendas();
    }
}
