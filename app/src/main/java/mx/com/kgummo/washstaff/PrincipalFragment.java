package mx.com.kgummo.washstaff;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.desai.vatsal.mydynamictoast.MyDynamicToast;
import com.google.gson.Gson;
import com.rilixtech.materialfancybutton.MaterialFancyButton;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import mx.com.kgummo.washstaff.Clases.clsGlobales;
import mx.com.kgummo.washstaff.Clases.clsRepartidor;
import mx.com.kgummo.washstaff.Clases.clsServicioH;
import mx.com.kgummo.washstaff.Clases.clsServiciosH;
import mx.com.kgummo.washstaff.WebService.ws_GenericIntRequest;
import mx.com.kgummo.washstaff.WebService.ws_LoginRequest;
import mx.com.kgummo.washstaff.WebService.ws_RepartidorResult;
import mx.com.kgummo.washstaff.WebService.ws_ServicioHResult;
import mx.com.kgummo.washstaff.WebService.ws_ServiciosHResult;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class PrincipalFragment extends Fragment {

    public PrincipalFragment() {
        // Required empty public constructor
    }

    public static PrincipalFragment newInstance()
    {
        PrincipalFragment fragment = new PrincipalFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        MaterialFancyButton boton;

        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_principal, container, false);

        ImageView imgUsuario = (ImageView) v.findViewById(R.id.imgFoto);
        Picasso.with(v.getContext()).load(clsGlobales.gblRepartidor.LogoProveedor).into(imgUsuario);

        TextView lblNombreUsuario = (TextView) v.findViewById(R.id.lblNombreUsuario);
        lblNombreUsuario.setText(clsGlobales.gblRepartidor.Nombre);

        boton = (MaterialFancyButton) v.findViewById(R.id.cmdPendientesPickUp);
        boton.setOnClickListener(new View.OnClickListener()
        {
            @Override public void onClick(View v)
            {
                getServiciosPP gSer = new getServiciosPP();
                gSer.execute();
            }
        });

        boton = (MaterialFancyButton) v.findViewById(R.id.cmdPendientesDrop);
        boton.setOnClickListener(new View.OnClickListener()
        {
            @Override public void onClick(View v)
            {
                getServiciosPD gSer = new getServiciosPD();
                gSer.execute();
            }
        });

        boton = (MaterialFancyButton) v.findViewById(R.id.cmdHistorial);
        boton.setOnClickListener(new View.OnClickListener()
        {
            @Override public void onClick(View v)
            {
                getServicios gSer = new getServicios();
                gSer.execute();
            }
        });

        return v;
    }

    public void onResume() {

        super.onResume();

        //Actualización de datos
        ValidateUser datos = new ValidateUser(clsGlobales.gblLogin,clsGlobales.gblPassword);
        datos.execute();

    }

    public void CloseSession()
    {
        clsGlobales.gblRepartidor = null;
        Intent i = new Intent(getContext(),MainActivity.class);
        startActivity(i);
        getActivity().finish();
    }

    @SuppressLint("StaticFieldLeak")
    class getServicios extends AsyncTask<Void,Void,Boolean>
    {
        private ProgressDialog pDialog;
        private String result;

        getServicios(){}

        @Override
        protected void onPreExecute()
        {
            pDialog = new ProgressDialog(getContext());
            pDialog.setTitle("WashBer");
            pDialog.setMessage("Obteniendo servicios...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... params)
        {
            String url = clsGlobales.API_URL + clsGlobales.API_SERVICIOSR;
            OkHttpClient client = new OkHttpClient();
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            Gson h = new Gson();
            ws_GenericIntRequest oRequest = new ws_GenericIntRequest();

            try
            {
                oRequest.Valor = clsGlobales.gblRepartidor.IdRepartidor;
                String jsRequest = h.toJson(oRequest);
                RequestBody body = RequestBody.create(JSON, jsRequest);

                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                return true;
            }
            catch (Exception e) {
                e.printStackTrace();
                pDialog.dismiss();
                return false;
            }
        }

        protected void onPostExecute(final Boolean success)
        {
            clsServicioH ser;

            try
            {
                ws_ServiciosHResult oResult  = new Gson().fromJson(String.valueOf(result),ws_ServiciosHResult.class);
                if(success)
                {
                    if(oResult.Codigo == 100)
                    {
                        if(oResult.Servicios.size() == 0)
                        {
                            pDialog.dismiss();
                            MyDynamicToast.warningMessage(getContext(), "Aún no has atendido servicios.");
                        }
                        else
                        {
                            clsGlobales.gblServiciosHeader = new clsServiciosH();
                            clsGlobales.gblServiciosHeader.Servicios = new ArrayList<clsServicioH>();
                            //cl_Global.gblServiciosHeader.Servicios = oResult.Servicios;
                            for(int i= 0; i < oResult.Servicios.size(); i++)
                            {
                                ws_ServicioHResult r = oResult.Servicios.get(i);
                                ser = new clsServicioH();
                                ser.Estatus = r.Estatus;
                                ser.Fecha = r.Fecha;
                                ser.Folio = r.Folio;
                                ser.IdOrden = r.IdOrden;
                                clsGlobales.gblServiciosHeader.Servicios.add(ser);
                            }
                            pDialog.dismiss();
                            DisplayHistory();
                        }
                    }
                    else if(oResult.Codigo == 101)
                    {
                        MyDynamicToast.errorMessage(getContext(), "Tenemos un problema con la información, reintenta mas tarde.");
                        pDialog.dismiss();
                    }
                    else
                    {
                        MyDynamicToast.warningMessage(getContext(), oResult.Mensaje);
                        pDialog.dismiss();
                    }
                }
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                pDialog.dismiss();
            }
        }
    }

    public void DisplayHistory()
    {
        Intent i = new Intent(getContext(),HistorialActivity.class);
        startActivity(i);
    }

    @SuppressLint("StaticFieldLeak")
    class getServiciosPP extends AsyncTask<Void,Void,Boolean>
    {
        private ProgressDialog pDialog;
        private String result;

        getServiciosPP(){}

        @Override
        protected void onPreExecute()
        {
            pDialog = new ProgressDialog(getContext());
            pDialog.setTitle("WashBer");
            pDialog.setMessage("Obteniendo servicios...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... params)
        {
            String url = clsGlobales.API_URL + clsGlobales.API_SERVICIOSPPR;
            OkHttpClient client = new OkHttpClient();
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            Gson h = new Gson();
            ws_GenericIntRequest oRequest = new ws_GenericIntRequest();

            try
            {
                oRequest.Valor = clsGlobales.gblRepartidor.IdRepartidor;
                String jsRequest = h.toJson(oRequest);
                RequestBody body = RequestBody.create(JSON, jsRequest);

                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                return true;
            }
            catch (Exception e) {
                e.printStackTrace();
                pDialog.dismiss();
                return false;
            }
        }

        protected void onPostExecute(final Boolean success)
        {
            clsServicioH ser;

            try
            {
                ws_ServiciosHResult oResult  = new Gson().fromJson(String.valueOf(result),ws_ServiciosHResult.class);
                if(success)
                {
                    if(oResult.Codigo == 100)
                    {
                        if(oResult.Servicios.size() == 0)
                        {
                            pDialog.dismiss();
                            MyDynamicToast.warningMessage(getContext(), "No tienes servicios pendientes de pick-up.");
                        }
                        else
                        {
                            clsGlobales.gblServiciosHeader = new clsServiciosH();
                            clsGlobales.gblServiciosHeader.Servicios = new ArrayList<clsServicioH>();
                            //cl_Global.gblServiciosHeader.Servicios = oResult.Servicios;
                            for(int i= 0; i < oResult.Servicios.size(); i++)
                            {
                                ws_ServicioHResult r = oResult.Servicios.get(i);
                                ser = new clsServicioH();
                                ser.Estatus = r.Estatus;
                                ser.Fecha = r.Fecha;
                                ser.Folio = r.Folio;
                                ser.IdOrden = r.IdOrden;
                                ser.Cliente = r.Cliente;
                                ser.Piezas = r.Piezas;
                                ser.Prendas = r.Prendas;
                                clsGlobales.gblServiciosHeader.Servicios.add(ser);
                            }
                            pDialog.dismiss();
                            DisplayPickUp();
                        }
                    }
                    else if(oResult.Codigo == 101)
                    {
                        MyDynamicToast.errorMessage(getContext(), "Tenemos un problema con la información, reintenta mas tarde.");
                        pDialog.dismiss();
                    }
                    else
                    {
                        MyDynamicToast.warningMessage(getContext(), oResult.Mensaje);
                        pDialog.dismiss();
                    }
                }
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                pDialog.dismiss();
            }
        }
    }

    public void DisplayPickUp()
    {
        Intent i = new Intent(getContext(),PickupActivity.class);
        startActivity(i);
    }

    @SuppressLint("StaticFieldLeak")
    class getServiciosPD extends AsyncTask<Void,Void,Boolean>
    {
        private ProgressDialog pDialog;
        private String result;

        getServiciosPD(){}

        @Override
        protected void onPreExecute()
        {
            pDialog = new ProgressDialog(getContext());
            pDialog.setTitle("WashBer");
            pDialog.setMessage("Obteniendo servicios...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... params)
        {
            String url = clsGlobales.API_URL + clsGlobales.API_SERVICIOSPDR;
            OkHttpClient client = new OkHttpClient();
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            Gson h = new Gson();
            ws_GenericIntRequest oRequest = new ws_GenericIntRequest();

            try
            {
                oRequest.Valor = clsGlobales.gblRepartidor.IdRepartidor;
                String jsRequest = h.toJson(oRequest);
                RequestBody body = RequestBody.create(JSON, jsRequest);

                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                return true;
            }
            catch (Exception e) {
                e.printStackTrace();
                pDialog.dismiss();
                return false;
            }
        }

        protected void onPostExecute(final Boolean success)
        {
            clsServicioH ser;

            try
            {
                ws_ServiciosHResult oResult  = new Gson().fromJson(String.valueOf(result),ws_ServiciosHResult.class);
                if(success)
                {
                    if(oResult.Codigo == 100)
                    {
                        if(oResult.Servicios.size() == 0)
                        {
                            pDialog.dismiss();
                            MyDynamicToast.warningMessage(getContext(), "No tienes servicios pendientes de drop-off.");
                        }
                        else
                        {
                            clsGlobales.gblServiciosHeader = new clsServiciosH();
                            clsGlobales.gblServiciosHeader.Servicios = new ArrayList<clsServicioH>();
                            //cl_Global.gblServiciosHeader.Servicios = oResult.Servicios;
                            for(int i= 0; i < oResult.Servicios.size(); i++)
                            {
                                ws_ServicioHResult r = oResult.Servicios.get(i);
                                ser = new clsServicioH();
                                ser.Estatus = r.Estatus;
                                ser.Fecha = r.Fecha;
                                ser.Folio = r.Folio;
                                ser.IdOrden = r.IdOrden;
                                ser.Cliente = r.Cliente;
                                ser.Piezas = r.Piezas;
                                ser.Prendas = r.Prendas;
                                clsGlobales.gblServiciosHeader.Servicios.add(ser);
                            }
                            pDialog.dismiss();
                            DisplayDropOff();
                        }
                    }
                    else if(oResult.Codigo == 101)
                    {
                        MyDynamicToast.errorMessage(getContext(), "Tenemos un problema con la información, reintenta mas tarde.");
                        pDialog.dismiss();
                    }
                    else
                    {
                        MyDynamicToast.warningMessage(getContext(), oResult.Mensaje);
                        pDialog.dismiss();
                    }
                }
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                pDialog.dismiss();
            }
        }
    }

    public void DisplayDropOff()
    {
        Intent i = new Intent(getContext(),DropActivity.class);
        startActivity(i);
    }

    @SuppressLint("StaticFieldLeak")
    public class ValidateUser extends AsyncTask<Void,Void,Boolean>
    {
        private ProgressDialog pDialog;
        private final String pUser;
        private final String pPassword;
        private String result;

        ValidateUser(String user,String psw)
        {
            pUser = user.trim();
            pPassword = psw.trim();
        }

        @Override
        protected void onPreExecute()
        {
            pDialog = new ProgressDialog(getContext());
            pDialog.setTitle("WashBer");
            pDialog.setMessage("Procesando...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... params)
        {


            try
            {
                String url = clsGlobales.API_URL + clsGlobales.API_LOGINR;
                OkHttpClient client = new OkHttpClient();
                MediaType JSON = MediaType.parse("application/json; charset=utf-8");
                Gson h = new Gson();
                ws_LoginRequest oReques = new ws_LoginRequest();

                oReques.Login = pUser;
                oReques.Password = pPassword;
                String jsRequest = h.toJson(oReques);
                RequestBody body = RequestBody.create(JSON, jsRequest);

                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                return true;
            }
            catch (Exception e) {
                e.printStackTrace();
                pDialog.dismiss();
                MyDynamicToast.errorMessage(getContext(), "Tuvimos un problema en procesar la información, intenta nuevamente.");
                return false;
            }

        }

        protected void onPostExecute(final Boolean success)
        {
            ws_RepartidorResult oResult;
            try
            {
                oResult =new Gson().fromJson(String.valueOf(result),ws_RepartidorResult.class);
                if(success)
                {
                    if(oResult.Codigo == 100)
                    {
                        clsGlobales.gblRepartidor = new clsRepartidor();
                        clsGlobales.gblRepartidor.IdRepartidor = oResult.IdRepartidor;
                        clsGlobales.gblRepartidor.Nombre = oResult.Nombre;
                        clsGlobales.gblRepartidor.CorreoElectronico = oResult.CorreoElectronico;
                        clsGlobales.gblRepartidor.Fotografia = oResult.Fotografia;
                        clsGlobales.gblRepartidor.Proveedor = oResult.Proveedor;
                        clsGlobales.gblRepartidor.LogoProveedor = oResult.LogoProveedor;
                        clsGlobales.gblRepartidor.PendienteDrop = oResult.PendienteDrop;
                        clsGlobales.gblRepartidor.PendientePick = oResult.PendientePick;
                        clsGlobales.gblRepartidor.DropOff = oResult.DropOff;
                        clsGlobales.gblRepartidor.PickUp = oResult.PickUp;
                        clsGlobales.gblRepartidor.MontoMinimoServicio = oResult.MontoMinimoServicio;

                        pDialog.dismiss();
                    }
                    else if(oResult.Codigo == 102)
                    {
                        pDialog.dismiss();
                        MyDynamicToast.informationMessage(getContext(), oResult.Mensaje);
                    }
                    else
                    {
                        pDialog.dismiss();
                        MyDynamicToast.errorMessage(getContext(), oResult.Mensaje);
                    }
                }
                else
                {
                    pDialog.dismiss();
                    MyDynamicToast.errorMessage(getContext(), "Tuvimos un problema en procesar la información, intenta nuevamente.");
                }
            }
            catch (Exception ex)
            {
                pDialog.dismiss();
                MyDynamicToast.errorMessage(getContext(), "Tuvimos un problema en procesar la información, intenta nuevamente.");
            }
        }
    }

}
