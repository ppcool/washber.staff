package mx.com.kgummo.washstaff.Clases;

import java.util.ArrayList;

import mx.com.kgummo.washstaff.WebService.ws_TipoIncidenciaResult;

/**
 * Created by ppcoo on 02/02/2018.
 */

public class clsGlobales {

    private static clsGlobales instance;

    public static String API_URL = "http://www.washber.com.mx/Servicio/wbServicio.svc";
    public static String API_URLDESARROLLO = "http://washberdev.azurewebsites.net/Servicio/wbServicio.svc";
    public static String API_LOGINR = "/RLogin";
    public static String API_SERVICIOSR = "/RServicios";
    public static String API_SERVICIOSPPR = "/RServiciosPP";
    public static String API_SERVICIOSPDR = "/RServiciosPD";
    public static String API_SERVICIODETALLE = "/SDetalle";
    public static String API_PRENDASS = "/SPrendas";
    public static String API_SAVEP = "/SPickUp";
    public static String API_PRENDASD = "/SPrendasD";
    public static String API_SAVED = "/SDropOff";
    public static String API_CGENERAL = "/CGeneral";
    public static String API_CGENERALSERVICIO = "/CGeneralOrden";
    public static String API_TIPOSINCIDENCIA = "/TiposIncidenciaPD";
    public static String API_SAVEINCIDENCIAPICK = "/IncidenciaPick";
    public static String API_SAVEINCIDENCIADROP = "/IncidenciaDrop";

    public static String gblLogin;
    public static  String gblPassword;
    public static clsGeneralCliente gblGeneralCliente;

    public static clsRepartidor gblRepartidor;

    public static clsServiciosH gblServiciosHeader;
    public static clsServicioH gblServicioHeader;
    public static clsDetalleServicio gblDetalleServicio;

    public static clsPrendas gblPrendasS;
    public static ArrayList<clsPrenda> gblPrendasP;

    public static ws_TipoIncidenciaResult gblTiposIncidencia;
}
