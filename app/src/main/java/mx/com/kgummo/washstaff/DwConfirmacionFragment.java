package mx.com.kgummo.washstaff;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.desai.vatsal.mydynamictoast.MyDynamicToast;
import com.google.gson.Gson;
import com.kofigyan.stateprogressbar.StateProgressBar;
import com.rilixtech.materialfancybutton.MaterialFancyButton;
import com.squareup.picasso.Picasso;

import mx.com.kgummo.washstaff.Adaptadores.adaPrendaItemd;
import mx.com.kgummo.washstaff.Clases.clsGlobales;
import mx.com.kgummo.washstaff.Clases.clsPrenda;
import mx.com.kgummo.washstaff.WebService.ws_GenericResult;
import mx.com.kgummo.washstaff.WebService.ws_SaveDropOffRequest;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class DwConfirmacionFragment extends Fragment {

    clsPrenda oPrenda;

    public DwConfirmacionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        TextView lblEtiqueta;
        Button goNextButton;
        MaterialFancyButton goBackButton;
        MaterialFancyButton cancelButton;
        MaterialFancyButton confirmButton;

        adaPrendaItemd aPrendas;
        ListView lvPrendas;

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dw_confirmacion, container, false);

        //TOOLBAR
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText("Confirmación de Drop-Off");

        //BARRA DE PROGRESO
        StateProgressBar stateProgressBar = (StateProgressBar) view.findViewById(R.id.pgrBar);
        stateProgressBar.setCurrentStateNumber(StateProgressBar.StateNumber.THREE);

        //INFORMACIÖN
        lblEtiqueta = (TextView) view.findViewById(R.id.lblFolio);
        lblEtiqueta.setText("Folio: " + clsGlobales.gblServicioHeader.Folio);

        lblEtiqueta = (TextView) view.findViewById(R.id.lblCliente);
        lblEtiqueta.setText(clsGlobales.gblServicioHeader.Cliente);

        lblEtiqueta = (TextView) view.findViewById(R.id.lblFechaSolicitada);
        lblEtiqueta.setText("Fecha solicitada: " + clsGlobales.gblDetalleServicio.FechaSolicitadaD.toString());

        lblEtiqueta = (TextView) view.findViewById(R.id.lblFechaProgramada);
        lblEtiqueta.setText("Fecha programada: " + clsGlobales.gblDetalleServicio.FechaProgramadaD.toString());


        lblEtiqueta = (TextView) view.findViewById(R.id.lblPrendasPiezas);
        lblEtiqueta.setText("Prendas: " + clsGlobales.gblServicioHeader.Prendas.toString() + ", piezas: "
                + clsGlobales.gblServicioHeader.Piezas.toString());

        lblEtiqueta = (TextView) view.findViewById(R.id.lblReferencia);
        lblEtiqueta.setText("Referencia: " + clsGlobales.gblDetalleServicio.Referencia.toString());

        final EditText txtObservaciones = (EditText) view.findViewById(R.id.txtObservaciones);

        //BOTONES
        goNextButton = (Button) view.findViewById(R.id.goNextButton);
        goBackButton = (MaterialFancyButton) view.findViewById(R.id.goBackButton);
        goNextButton.setVisibility(View.GONE);

        goBackButton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                ((DropoffWizardActivity) getActivity()).getWizard().navigatePrevious();
            }
        });

        cancelButton = (MaterialFancyButton) view.findViewById(R.id.cmdCancelar);
        cancelButton.setOnClickListener(new View.OnClickListener()
        {
            @Override public void onClick(View v)
            {
                Cancel();
            }
        });

        confirmButton = (MaterialFancyButton) view.findViewById(R.id.cmdConfirmar);
        confirmButton.setOnClickListener(new View.OnClickListener()
        {
            @Override public void onClick(View v)
            {
                saveDropOff save = new saveDropOff(clsGlobales.gblServicioHeader.IdOrden
                        ,clsGlobales.gblRepartidor.IdRepartidor,txtObservaciones.getText().toString().trim());

                save.execute();
            }
        });

        //LISTA DE PRENDAS
        lvPrendas = (ListView) view.findViewById(R.id.lstPrendas);
        aPrendas = new adaPrendaItemd(getContext(), clsGlobales.gblPrendasS.Prendas);
        lvPrendas.setAdapter(aPrendas);

        lvPrendas.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                oPrenda = clsGlobales.gblPrendasS.Prendas.get(i);
                GetPicture();
            }
        });

        return view;
    }

    public void  Cancel()
    {
        Intent intent = new Intent(getActivity(),DropActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    public void GetPicture()
    {
        final Dialog dialog  = new Dialog(getActivity());
        ImageView imgFoto;

        dialog.setContentView(R.layout.layout_modalpicture);
        dialog.setTitle("Fotografía de prenda");
        imgFoto = (ImageView) dialog.findViewById(R.id.imgImagen);

        Picasso.with(getContext()).load(oPrenda.Imagen).into(imgFoto);

        //EVENTO DE BOTÓN DE CERRAR
        MaterialFancyButton Cancelar = (MaterialFancyButton) dialog.findViewById(R.id.cmdCerrar);
        Cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    class saveDropOff extends AsyncTask<Void,Void, Boolean>
    {
        private ProgressDialog pDialog;
        private String result;
        private Integer idOrden;
        private Integer idRepartidor;
        private String observaciones;

        saveDropOff(Integer pIdOrden, Integer pIdRepartidor, String pObservaciones)
        {
            this.idOrden = pIdOrden;
            this.idRepartidor = pIdRepartidor;
            this.observaciones = pObservaciones;
        }

        @Override
        protected void onPreExecute()
        {
            pDialog = new ProgressDialog(getActivity());
            pDialog.setTitle("WashBer");
            pDialog.setMessage("Procesando...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... params)
        {
            String url = clsGlobales.API_URL + clsGlobales.API_SAVED;
            OkHttpClient client = new OkHttpClient();
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            Gson h = new Gson();
            ws_SaveDropOffRequest oRequest = new ws_SaveDropOffRequest();

            try
            {
                oRequest.IdOrden = idOrden;
                oRequest.IdRepartidor = idRepartidor;
                oRequest.Observaciones = observaciones;

                String jsRequest = h.toJson(oRequest);
                RequestBody body = RequestBody.create(JSON, jsRequest);
                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                return true;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                pDialog.dismiss();
                return false;
            }
        }

        protected void onPostExecute(final Boolean success)
        {
            try
            {
                ws_GenericResult oResult = new Gson().fromJson(String.valueOf(result),ws_GenericResult.class);
                if(success)
                {
                    if(oResult.Codigo == 100)
                    {
                        MyDynamicToast.successMessage(getActivity(), "Drop-off registrado.");
                        pDialog.dismiss();
                        closeWizard();
                    }
                    else if (oResult.Codigo == 102)
                    {
                        MyDynamicToast.informationMessage(getActivity(), oResult.Mensaje);
                        pDialog.dismiss();
                    }
                    else
                    {
                        MyDynamicToast.errorMessage(getActivity(), "Tuvimos un problema al registrar tu servicio, intenta nuevamente.");
                        pDialog.dismiss();
                    }
                }
            }
            catch (Exception ex)
            {
                MyDynamicToast.errorMessage(getActivity(), "Tuvimos un problema al registrar el Drop-Off, intenta nuevamente.");
            }
        }
    }

    private void closeWizard()
    {
        Intent i = new Intent(getContext(),PrincipalActivity.class);
        startActivity(i);
        getActivity().finish();
    }
}
