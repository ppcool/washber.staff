package mx.com.kgummo.washstaff.Clases;

/**
 * Created by ppcoo on 07/02/2018.
 */

public class clsItemCheckList {
    String Cadena;
    Integer Id;
    boolean Checkox;

    public clsItemCheckList(){}

    public clsItemCheckList(String pCadena, Integer pId, boolean status)
    {
        this.Cadena = pCadena;
        this.Id = pId;
        this.Checkox = status;
    }

    public String getCadena()
    {
        return Cadena;
    }

    public void setCadena(String pCadena)
    {
        this.Cadena = pCadena;
    }

    public Integer getId()
    {
        return Id;
    }

    public void setId(Integer pId)
    {
        this.Id = pId;
    }

    public boolean isCheckox()
    {
        return Checkox;
    }

    public void setCheckox(boolean status)
    {
        this.Checkox = status;
    }
}
