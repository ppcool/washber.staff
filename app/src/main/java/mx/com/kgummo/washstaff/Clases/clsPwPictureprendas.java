package mx.com.kgummo.washstaff.Clases;

import me.panavtec.wizard.WizardPage;
import mx.com.kgummo.washstaff.PwPictureprendasFragment;

/**
 * Created by ppcoo on 07/02/2018.
 */

public class clsPwPictureprendas extends WizardPage<PwPictureprendasFragment> {

    @Override
    public PwPictureprendasFragment createFragment()
    {
        return new PwPictureprendasFragment();
    }
}
