package mx.com.kgummo.washstaff;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.kofigyan.stateprogressbar.StateProgressBar;
import com.rilixtech.materialfancybutton.MaterialFancyButton;
import com.squareup.picasso.Picasso;

import mx.com.kgummo.washstaff.Adaptadores.adaPrendaItemd;
import mx.com.kgummo.washstaff.Clases.clsGlobales;
import mx.com.kgummo.washstaff.Clases.clsPrenda;


/**
 * A simple {@link Fragment} subclass.
 */
public class DwPrendasFragment extends Fragment {

    private MaterialFancyButton goNextButton;
    private MaterialFancyButton goBackButton;
    private MaterialFancyButton cancelButton;

    private adaPrendaItemd aPrendas;
    ListView lvPrendas;
    clsPrenda oPrenda;

    public DwPrendasFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dw_prendas, container, false);

        //TOOLBAR
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText("Fotografia de prenda");

        //BARRA DE PROGRESO
        StateProgressBar stateProgressBar = (StateProgressBar) view.findViewById(R.id.pgrBar);
        stateProgressBar.setCurrentStateNumber(StateProgressBar.StateNumber.TWO);

        //BOTONES
        goNextButton = (MaterialFancyButton) view.findViewById(R.id.goNextButton);
        goNextButton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                ((DropoffWizardActivity) getActivity()).getWizard().navigateNext();
            }
        });

        goBackButton = (MaterialFancyButton) view.findViewById(R.id.goBackButton);
        goBackButton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                ((DropoffWizardActivity) getActivity()).getWizard().navigatePrevious();
            }
        });

        cancelButton = (MaterialFancyButton) view.findViewById(R.id.cmdCancelar);
        cancelButton.setOnClickListener(new View.OnClickListener()
        {
            @Override public void onClick(View v)
            {
                Cancel();
            }
        });

        //LISTA DE PRENDAS
        lvPrendas = (ListView) view.findViewById(R.id.lstPrendas);
        aPrendas = new adaPrendaItemd(getContext(), clsGlobales.gblPrendasS.Prendas);
        lvPrendas.setAdapter(aPrendas);

        lvPrendas.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                oPrenda = clsGlobales.gblPrendasS.Prendas.get(i);
                GetPicture();
            }
        });

        return view;
    }

    public void  Cancel()
    {
        Intent intent = new Intent(getActivity(),DropActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    public void GetPicture()
    {
        final Dialog dialog  = new Dialog(getActivity());
        ImageView imgFoto;

        dialog.setContentView(R.layout.layout_modalpicture);
        dialog.setTitle("Fotografía de prenda");
        imgFoto = (ImageView) dialog.findViewById(R.id.imgImagen);

        Picasso.with(getContext()).load(oPrenda.Imagen).into(imgFoto);

        //EVENTO DE BOTÓN DE CERRAR
        MaterialFancyButton Cancelar = (MaterialFancyButton) dialog.findViewById(R.id.cmdCerrar);
        Cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

}
