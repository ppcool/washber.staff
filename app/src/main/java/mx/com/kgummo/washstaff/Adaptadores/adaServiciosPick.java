package mx.com.kgummo.washstaff.Adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import mx.com.kgummo.washstaff.Clases.clsServicioH;
import mx.com.kgummo.washstaff.R;

/**
 * Created by ppcoo on 07/02/2018.
 */

public class adaServiciosPick extends ArrayAdapter<clsServicioH> {

    public adaServiciosPick (Context context, ArrayList<clsServicioH> elementos)
    {
        super(context,0,elementos);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        clsServicioH elemento = getItem(position);

        if(convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.layout_servicioheader,parent,false);
        }

        TextView Folio = (TextView) convertView.findViewById(R.id.txtFolio);
        TextView Fecha = (TextView) convertView.findViewById(R.id.txtFecha);
        TextView Estatus = (TextView) convertView.findViewById(R.id.txtEstatus);

        Folio.setText("Folio:  "+ elemento.Folio);
        Fecha.setText("Pick-Up: " + elemento.Fecha);
        Estatus.setText("Cliente: " + elemento.Cliente);

        return convertView;
    }
}
