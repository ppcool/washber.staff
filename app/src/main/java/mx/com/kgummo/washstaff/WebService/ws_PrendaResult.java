package mx.com.kgummo.washstaff.WebService;

/**
 * Created by ppcoo on 07/02/2018.
 */

public class ws_PrendaResult {
    public String Categoria;
    public String Fotografia;
    public Integer IdPrendaOrden;
    public Integer IdSubcategoria;
    public Integer Piezas;
    public float Precio;
    public String Subcategoria;
}
