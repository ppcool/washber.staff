package mx.com.kgummo.washstaff.Adaptadores;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import mx.com.kgummo.washstaff.Clases.clsServicioH;
import mx.com.kgummo.washstaff.R;

/**
 * Created by ppcoo on 02/02/2018.
 */

public class adaServicioheader extends ArrayAdapter<clsServicioH> {

    public adaServicioheader (Context context, ArrayList<clsServicioH> elementos)
    {
        super(context,0,elementos);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent)
    {
        clsServicioH elemento = getItem(position);
        String cadena;
        if(convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.layout_servicioheader,parent,false);
        }

        TextView Folio = (TextView) convertView.findViewById(R.id.txtFolio);
        TextView Fecha = (TextView) convertView.findViewById(R.id.txtFecha);
        TextView Estatus = (TextView) convertView.findViewById(R.id.txtEstatus);

        assert elemento != null;
        cadena = "Folio:  "+ elemento.Folio;
        Folio.setText(cadena);

        cadena = "Registrado: " + elemento.Fecha;
        Fecha.setText(cadena);
        Estatus.setText(elemento.Estatus);

        return convertView;
    }
}
