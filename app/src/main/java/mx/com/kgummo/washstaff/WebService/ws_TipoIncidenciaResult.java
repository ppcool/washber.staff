package mx.com.kgummo.washstaff.WebService;

import java.util.ArrayList;

import mx.com.kgummo.washstaff.Clases.clsTipoIncidencia;

/**
 * Created by ppcoo on 06/03/2018.
 */

public class ws_TipoIncidenciaResult {
    public Integer Codigo;
    public String Mensaje;
    public ArrayList<clsTipoIncidencia> Incidencias;
}
